package vtc.project.instanthelpers.activitys;

import java.io.IOException;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import vtc.project.instanthelpers.Ih_Apps;
import vtc.project.instanthelpers.R;
import vtc.project.instanthelpers.fragments.Ih_HelperHomeFragment;
import vtc.project.instanthelpers.fragments.Ih_HelperOrderFragment;
import vtc.project.instanthelpers.fragments.Ih_HelperOrderFragment2;
import vtc.project.instanthelpers.fragments.Ih_HelperProfileFragment;
import vtc.project.instanthelpers.ihmworkerendpoint.Ihmworkerendpoint;
import vtc.project.instanthelpers.ihmworkerendpoint.model.IhMWorker;

public class Ih_HelperDrawerActivity extends Ih_BaseActivity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle drawerListener;
	private MyAdapter myAdapter;

	private OnItemClickListener mDrawerClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_drawer_helper);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerListener = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);

			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);

			}
		};
		mDrawerLayout.setDrawerListener(drawerListener);

		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		myAdapter = new MyAdapter(this);
		mDrawerList.setAdapter(myAdapter);

		/*
		 * mDrawerList.setAdapter(new ArrayAdapter<>(this,
		 * android.R.layout.simple_list_item_1, mNavigationDrawerItemTitles));
		 */
		mDrawerList.setOnItemClickListener(mDrawerClickListener);
		selectItem(0);
		mDrawerList.setItemChecked(0, true);
		mDrawerList.setSelection(0);
		int SDK_INT = android.os.Build.VERSION.SDK_INT;
		if (SDK_INT > 8) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
	}

	public void selectItem(int position) {

		getActionBar().setTitle(getResources().getStringArray(R.array.nav_helper_drawer_titles)[position]);

		Fragment fragment = null;
		Intent mainIntent = new Intent(this, Ih_LoginActivity.class);
		switch (position) {
		case 0:
			fragment = new Ih_HelperHomeFragment();
			break;
		case 1:
			fragment = new Ih_HelperOrderFragment();
			break;
		case 2:
			fragment = new Ih_HelperOrderFragment2();
			break;
		case 3:
			addWorker();
			break;
		case 4:
			fragment = new Ih_HelperProfileFragment();
			break;
		case 5:
			mainIntent.putExtra("Action", "SignOut");
			startActivity(mainIntent);
			finish();
			break;
		case 6:
			mainIntent.putExtra("Action", "Revoke");
			startActivity(mainIntent);
			finish();
			break;
		}
		if (fragment != null) {
			FragmentManager fragmentManager = getFragmentManager();
			FragmentTransaction transaction = fragmentManager.beginTransaction();
			transaction.replace(R.id.content_frame, fragment);

			// if (fragment.getClass().getName() !=
			// "vtc.project.instanthelpers.a.MainActivity") {
			// transaction.addToBackStack(fragment.getClass().getName());
			// Log.i("addToBackStack", fragment.getClass().getName());
			// }
			transaction.commit();
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
		}
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	private void addWorker() {
		final Context context = this;
		final Dialog dialog = new Dialog(context);
		dialog.setTitle("Add Worker::");
		dialog.setCancelable(false);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		LinearLayout layout = new LinearLayout(context);
		layout.setLayoutParams(params);
		layout.setOrientation(LinearLayout.VERTICAL);
		TextView workerlabel = new TextView(context);
		workerlabel.setText("Worker Id:");
		workerlabel.setLayoutParams(params);
		final EditText workerid = new EditText(context);
		workerid.setLayoutParams(params);
		workerid.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
		workerid.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
		Button submit = new Button(context);
		submit.setText("Submit");
		submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AsyncTask<Void, Void, IhMWorker> task = new AsyncTask<Void, Void, IhMWorker>() {
					Ihmworkerendpoint ihmwEndpoint = Ih_Apps.getmWorkerEndpoint();
					IhMWorker ihmw;
					@Override
					protected IhMWorker doInBackground(Void... params) {
						try {
							ihmw = ihmwEndpoint.getIhMWorker(Long.parseLong(workerid.getText().toString())).execute();
							if(ihmw!=null){
								ihmw.setMHelperId(Ih_Apps.getIhmaccount().getId());
								ihmwEndpoint.updateIhMWorker(ihmw).execute();
							}
						} catch (NumberFormatException | IOException e) {
							e.printStackTrace();
						}
						return ihmw;
					}
					@Override
					protected void onPostExecute(IhMWorker result) {
						super.onPostExecute(result);
						if(result!=null){
							Toast.makeText(context, "Worker added",Toast.LENGTH_SHORT).show();
						}else{
							Toast.makeText(context, "Worker not found",Toast.LENGTH_SHORT).show();
						}
						if(dialog.isShowing()){
							dialog.dismiss();
						}
					}
				};
				task.execute();
			}
		});
		Button cancel = new Button(context);
		cancel.setText("Cancel");
		cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		layout.addView(workerlabel);
		layout.addView(workerid);
		layout.addView(submit);
		layout.addView(cancel);
		dialog.setContentView(layout);
		dialog.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.drawer, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

		if (drawerListener.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerListener.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawerListener.syncState();
	}

	private class MyAdapter extends BaseAdapter {
		String[] titles;
		TypedArray images;
		private Context context;

		public MyAdapter(Context context) {
			this.context = context;

			titles = context.getResources().getStringArray(R.array.nav_helper_drawer_titles);
			images = context.getResources().obtainTypedArray(R.array.nav_helper_drawer_icons);
		}

		@Override
		public int getCount() {
			return titles.length;
		}

		@Override
		public Object getItem(int position) {
			return titles[position];
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = null;
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = inflater.inflate(R.layout.drawer_item_row, parent, false);
			} else {
				row = convertView;
			}
			TextView titleTextView = (TextView) row.findViewById(R.id.textView1);
			ImageView titleImageView = (ImageView) row.findViewById(R.id.imageView1);
			titleTextView.setText(titles[position]);
			titleImageView.setImageResource(images.getResourceId(position, -1));

			return row;
		}
	}
}
