package vtc.project.instanthelpers.activitys;

import java.io.IOException;
import java.io.InputStream;

import vtc.project.instanthelpers.GCMIntentService;
import vtc.project.instanthelpers.Ih_Apps;
import vtc.project.instanthelpers.R;
import vtc.project.instanthelpers.ihmaccountendpoint.Ihmaccountendpoint;
import vtc.project.instanthelpers.ihmaccountendpoint.model.CollectionResponseIhMAccount;
import vtc.project.instanthelpers.ihmaccountendpoint.model.IhMAccount;
import vtc.project.instanthelpers.util.SystemUiHider;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class Ih_LoginActivity extends Ih_BaseActivity {
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
	 */
	private static final boolean AUTO_HIDE = true;

	/**
	 * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
	 * user interaction before hiding the system UI.
	 */
	private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

	/**
	 * If set, will toggle the system UI visibility upon interaction. Otherwise,
	 * will show the system UI visibility upon interaction.
	 */
	private static final boolean TOGGLE_ON_CLICK = true;

	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	private SystemUiHider mSystemUiHider;

	public static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	private Dialog mAccountDialog;
	private Button mSignOutButton;
	private SignInButton mSignInButton;

	private OnClickListener mSignIn = new OnClickListener() {
		@Override
		public void onClick(View v) {
			mSignInButton.setVisibility(View.GONE);
			mSignOutButton.setVisibility(View.GONE);
			resolveSignInError();
		}

	};

	private OnClickListener mSignOut = new OnClickListener() {
		@Override
		public void onClick(View v) {
			SignOut();
		}
	};

	private OnClickListener mRevoke = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Revoke();
		}
	};

	private OnClickListener mAccountDialogDismiss = new OnClickListener() {
		@Override
		public void onClick(View v) {
			mAccountDialog.dismiss();
		}
	};

	private OnClickListener mAccountDialogCustomer = new OnClickListener() {
		@Override
		public void onClick(View v) {
			InsertAccount("Customer");
			mAccountDialog.dismiss();
		}
	};
	private OnClickListener mAccountDialogHelper = new OnClickListener() {
		@Override
		public void onClick(View v) {
			InsertAccount("Helper");
			mAccountDialog.dismiss();
		}
	};

	private OnClickListener mAccountDialogWorker = new OnClickListener() {
		@Override
		public void onClick(View v) {
			InsertAccount("Worker");
			mAccountDialog.dismiss();
		}

	};
	
	private OnClickListener mAccountDialogShow = new OnClickListener() {
		@Override
		public void onClick(View v) {
			mAccountDialog.show();
		}
	};

	private void SignOut() {
		if (mGoogleApiClient.isConnected()) {
			Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			mGoogleApiClient.disconnect();
			mGoogleApiClient.connect();
		}
	}

	private void Revoke() {
		if (mGoogleApiClient.isConnected()) {
			Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
				@Override
				public void onResult(Status arg0) {
				}
			});
		}
		startActivity(new Intent(Ih_LoginActivity.this, Ih_LoginActivity.class));
		finish();
	}

	protected GoogleApiClient buildGoogleApiClient() {
		return new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this)
				.addApi(Plus.API, Plus.PlusOptions.builder().build()).addScope(Plus.SCOPE_PLUS_LOGIN).build();
	}

	private void InsertAccount(String type) {
		IhMAccount account = new IhMAccount();
		account.setMEmail(Plus.AccountApi.getAccountName(mGoogleApiClient));
		account.setMAccountType(type);
		account.setMNickName(Plus.PeopleApi.getCurrentPerson(mGoogleApiClient).getDisplayName());
		if (GCMIntentService.PROJECT_NUMBER == null || GCMIntentService.PROJECT_NUMBER.length() == 0) {
			showDialog("Unable to register for Google Cloud Messaging. "
					+ "Your application's PROJECT_NUMBER field is unset! You can change "
					+ "it in GCMIntentService.java");
		} else {

			try {
				GCMIntentService.register(getApplicationContext(), account, credential);
			} catch (Exception e) {
				Log.e(Ih_LoginActivity.class.getName(),
						"Exception received when attempting to register for Google Cloud "
								+ "Messaging. Perhaps you need to set your virtual device's "
								+ " target to Google APIs? "
								+ "See https://developers.google.com/eclipse/docs/cloud_endpoints_android"
								+ " for more information.",
						e);
				showDialog("There was a problem when attempting to register for "
						+ "Google Cloud Messaging. If you're running in the emulator, "
						+ "is the target of your virtual device set to 'Google APIs?' "
						+ "See the Android log for more details.");

			}
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		/*
		 * If we are dealing with an intent generated by the GCMIntentService
		 * class, then display the provided message.
		 */
		if (intent.getBooleanExtra("gcmIntentServiceMessage", false)) {

			showDialog(intent.getStringExtra("message"));

			if (intent.getBooleanExtra("registrationMessage", false)) {

				if (intent.getBooleanExtra("error", false)) {
					/*
					 * If we get a registration/unregistration-related error,
					 * and we're in the process of registering, then we move
					 * back to the unregistered state. If we're in the process
					 * of unregistering, then we move back to the registered
					 * state.
					 */

				} else {
					/*
					 * If we get a registration/unregistration-related success,
					 * and we're in the process of registering, then we move to
					 * the registered state. If we're in the process of
					 * unregistering, the we move back to the unregistered
					 * state.
					 */
					new QueryAccountTask(Ih_LoginActivity.this, mAccountEndpoint, accountName).execute();
				}
			} else {
				/*
				 * if we didn't get a registration/unregistration message then
				 * go get the last 5 messages from app-engine
				 */

			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(TAG, "mSignInProgress:" + mSignInProgress);
		setContentView(R.layout.activity_login);
		mSignInButton = (SignInButton) findViewById(R.id.sign_in_button);
		mSignOutButton = (Button) findViewById(R.id.sign_out_button);
		mSignInButton.setVisibility(View.GONE);
		mSignOutButton.setVisibility(View.GONE);
		mSignInButton.setOnClickListener(mSignIn);
		mSignOutButton.setOnClickListener(mAccountDialogShow);

		final View controlsView = findViewById(R.id.fullscreen_content_controls);
		final View contentView = findViewById(R.id.fullscreen_content);

		// Set up an instance of SystemUiHider to control the system UI for
		// this activity.
		mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
		mSystemUiHider.setup();
		mSystemUiHider.setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
			// Cached values.
			int mControlsHeight;
			int mShortAnimTime;

			@Override
			@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
			public void onVisibilityChange(boolean visible) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
					// If the ViewPropertyAnimator API is available
					// (Honeycomb MR2 and later), use it to animate the
					// in-layout UI controls at the bottom of the
					// screen.
					if (mControlsHeight == 0) {
						mControlsHeight = controlsView.getHeight();
					}
					if (mShortAnimTime == 0) {
						mShortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
					}
					controlsView.animate().translationY(visible ? 0 : mControlsHeight).setDuration(mShortAnimTime);
				} else {
					// If the ViewPropertyAnimator APIs aren't
					// available, simply show or hide the in-layout UI
					// controls.
					controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
				}

				if (visible && AUTO_HIDE) {
					// Schedule a hide().
					delayedHide(AUTO_HIDE_DELAY_MILLIS);
				}
			}
		});

		// Set up the user interaction to manually show or hide the system UI.
		contentView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (TOGGLE_ON_CLICK) {
					mSystemUiHider.toggle();
				} else {
					mSystemUiHider.show();
				}
			}
		});

		// Upon interacting with UI controls, delay any scheduled hide()
		// operations to prevent the jarring behavior of controls going away
		// while interacting with the UI.
		findViewById(R.id.sign_in_button).setOnTouchListener(mDelayHideTouchListener);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		// Trigger the initial hide() shortly after the activity has been
		// created, to briefly hint to the user that UI controls
		// are available.
		delayedHide(5000);
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		super.onConnected(connectionHint);
		new QueryAccountTask(this, mAccountEndpoint, accountName).execute();
		if (getIntent().getStringExtra("Action") != null) {
			if (getIntent().getStringExtra("Action").equals("SignOut")) {
				SignOut();
			}
			if (getIntent().getStringExtra("Action").equals("Revoke")) {
				Revoke();
			}
			getIntent().removeExtra("Action");
			mSignInButton.setVisibility(View.VISIBLE);
			mSignOutButton.setVisibility(View.GONE);
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		super.onConnectionFailed(result);
		mSignInButton.setVisibility(View.VISIBLE);
		mSignOutButton.setVisibility(View.GONE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case RC_SIGN_IN:
			if (resultCode == RESULT_OK) {
				Log.i(TAG, "resolvSignInError():resultCode == RESULT_OK");
				mSignInProgress = STATE_SIGN_IN;
			} else {
				mSignInProgress = STATE_DEFAULT;
				Log.i(TAG, "onActivityResult 01: mSignInProgress = STATE_DEFAULT resultCode: " + resultCode);
			}

			if (!mGoogleApiClient.isConnected()) {
				mGoogleApiClient.connect();
				Log.i(TAG, "onActivityResult 02: !mGoogleApiClient.isConnected()");
			}
			break;
		case REQUEST_ACCOUNT_PICKER:
			if (data != null && data.getExtras() != null) {

				accountName = data.getExtras().getString(AccountManager.KEY_ACCOUNT_NAME);
				if (accountName != null) {
					credential.setSelectedAccountName(accountName);

				}
			}
			break;

		}
	}

	/**
	 * Touch listener to use for in-layout UI controls to delay hiding the
	 * system UI. This is to prevent the jarring behavior of controls going away
	 * while interacting with activity UI.
	 */
	View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent motionEvent) {
			if (AUTO_HIDE) {
				delayedHide(AUTO_HIDE_DELAY_MILLIS);
			}
			return false;
		}
	};

	Handler mHideHandler = new Handler();
	Runnable mHideRunnable = new Runnable() {
		@Override
		public void run() {
			mSystemUiHider.hide();
		}
	};

	/**
	 * Schedules a call to hide() in [delay] milliseconds, canceling any
	 * previously scheduled calls.
	 */
	private void delayedHide(int delayMillis) {
		mHideHandler.removeCallbacks(mHideRunnable);
		mHideHandler.postDelayed(mHideRunnable, delayMillis);
	}

	/**
	 * Background Async task to load user profile picture from url
	 */
	private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public LoadProfileImage(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}
	}

	private void chooseDrawer(String type) {
		switch (type) {
		case "Customer":
			startActivity(new Intent(Ih_LoginActivity.this, Ih_CustomerDrawerActivity.class));
			finish();
			break;
		case "Helper":
			startActivity(new Intent(Ih_LoginActivity.this, Ih_HelperDrawerActivity.class));
			finish();
			break;
		case "Worker":
			startActivity(new Intent(Ih_LoginActivity.this, Ih_WorkerDrawerActivity.class));
			finish();
			break;
		}
	}

	class QueryAccountTask extends AsyncTask<Void, Void, CollectionResponseIhMAccount> {
		Exception exceptionThrown = null;
		Ihmaccountendpoint accountEndpoint;
		String email;
		Activity activity;

		public QueryAccountTask(Activity activity, Ihmaccountendpoint accountEndpoint, String email) {
			this.accountEndpoint = accountEndpoint;
			this.email = email;
			this.activity = activity;
		}

		@Override
		protected CollectionResponseIhMAccount doInBackground(Void... params) {
			try {
				CollectionResponseIhMAccount accounts = accountEndpoint.getIhMAccountByEmail(email).execute();
				return accounts;
			} catch (IOException e) {
				exceptionThrown = e;
				return null;
				// Handle exception in PostExecute
			}
		}

		protected void onPostExecute(CollectionResponseIhMAccount accounts) {
			if (exceptionThrown != null) {
				Log.e(Ih_LoginActivity.class.getName(), "Exception when QueryAccountTask", exceptionThrown);
				showDialog("Failed to retrieve account from " + "the endpoint at " + accountEndpoint.getBaseUrl()
						+ ", check log for details");
			} else {

				if (accounts.getItems() != null) {
					Log.i(TAG, "find " + accounts.getItems().size() + " accounts of " + email + " \n"
							+ accountEndpoint.getServicePath());
					if (mGoogleApiClient.isConnected()) {
						Ih_Apps.setIhmaccount(accounts.getItems().get(0));
						ihmaccount = Ih_Apps.getIhmaccount();
						chooseDrawer(ihmaccount.getMAccountType());
					}

				} else {
					mAccountDialog = new Dialog(Ih_LoginActivity.this);
					mAccountDialog.setContentView(R.layout.account_type);
					mAccountDialog.show();
					((Button) mAccountDialog.findViewById(R.id.buttonCustomer))
							.setOnClickListener(mAccountDialogCustomer);
					((Button) mAccountDialog.findViewById(R.id.buttonHelper)).setOnClickListener(mAccountDialogHelper);
					((Button) mAccountDialog.findViewById(R.id.buttonWorker)).setOnClickListener(mAccountDialogWorker);
					mSignInButton.setVisibility(View.GONE);
					mSignOutButton.setVisibility(View.VISIBLE);
				}

			}
		}
	}
}