package vtc.project.instanthelpers.activitys;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import vtc.project.instanthelpers.CloudEndpointUtils;
import vtc.project.instanthelpers.Ih_Apps;
import vtc.project.instanthelpers.R;
import vtc.project.instanthelpers.ihmaccountendpoint.Ihmaccountendpoint;
import vtc.project.instanthelpers.ihmaccountendpoint.model.CollectionResponseIhMAccount;
import vtc.project.instanthelpers.ihmaccountendpoint.model.IhMAccount;
import vtc.project.instanthelpers.ihmcustomerendpoint.Ihmcustomerendpoint;
import vtc.project.instanthelpers.ihmhelperendpoint.Ihmhelperendpoint;
import vtc.project.instanthelpers.ihmorderendpoint.Ihmorderendpoint;
import vtc.project.instanthelpers.ihmquotationendpoint.Ihmquotationendpoint;
import vtc.project.instanthelpers.ihmrequestendpoint.Ihmrequestendpoint;
import vtc.project.instanthelpers.ihmworkerendpoint.Ihmworkerendpoint;
import vtc.project.instanthelpers.util.Ih_Constants;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.jackson2.JacksonFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class Ih_BaseActivity extends Activity implements ConnectionCallbacks, OnConnectionFailedListener {
	protected int mSignInProgress;
	protected static final int STATE_DEFAULT = 0;
	protected static final int STATE_SIGN_IN = 1;
	protected static final int STATE_IN_PROGRESS = 2;
	protected static final int RC_SIGN_IN = 0;
	protected static final int DIALOG_PLAY_SERVICES_ERROR = 0;
	protected static final String SAVED_PROGRESS = "sign_in_progress";
	protected PendingIntent mSignInIntent;
	protected int mSignInError;
	protected static final String TAG = "vtc.project.instanthelpers";
	protected static final int REQUEST_ACCOUNT_PICKER = 1;

	protected GoogleApiClient mGoogleApiClient;
	protected GoogleAccountCredential credential;
	protected String accountName;
	protected IhMAccount ihmaccount;
	protected Ihmaccountendpoint mAccountEndpoint;
	protected Ihmrequestendpoint mRequestEndpoint;
	protected Ihmorderendpoint mOrderEndpoint;
	protected Ihmquotationendpoint mQuotationEndpoint;
	protected Ihmcustomerendpoint mCustomerEndpoint;
	protected Ihmhelperendpoint mHelperEndpoint;
	protected Ihmworkerendpoint mWorkerEndpoint;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Ih_Apps.setmGoogleApiClient(buildGoogleApiClient());
		this.mGoogleApiClient = Ih_Apps.getmGoogleApiClient();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Log.i(TAG, "onConnectionFailed 01: ConnectionResult.getErrorCode() = " + result.getErrorCode());

		if (mSignInProgress != STATE_IN_PROGRESS) {
			mSignInIntent = result.getResolution();
			mSignInError = result.getErrorCode();
			Log.i(TAG, "onConnectionFailed 02: mSignInProgress != STATE_IN_PROGRESS");

			if (mSignInProgress == STATE_SIGN_IN) {
				Log.i(TAG, "onConnectionFailed 03a: mSignInProgress == STATE_SIGN_IN:");
				resolveSignInError();
				Log.i(TAG, "onConnectionFailed 03b: after resolveSignInError()");
			}
		}

		onSignedOut();
	}

	@Override
	public void onConnected(Bundle connectionHint) {

		Ih_Apps.setAccountName(Plus.AccountApi.getAccountName(mGoogleApiClient));
		this.accountName = Ih_Apps.getAccountName();
		Log.i(TAG, "onConnected:" + accountName);

		mSignInProgress = STATE_DEFAULT;
		Ih_Apps.setCredential(
				GoogleAccountCredential.usingAudience(this, "server:client_id:" + Ih_Constants.WEB_CLIENT_ID));
		this.credential = Ih_Apps.getCredential();

		// startActivityForResult(credential.newChooseAccountIntent(),REQUEST_ACCOUNT_PICKER);

		credential.setSelectedAccountName(Plus.AccountApi.getAccountName(mGoogleApiClient));

		Ihmaccountendpoint.Builder endpointBuilder = new Ihmaccountendpoint.Builder(
				AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential);
		endpointBuilder.setApplicationName("InstantHelpers-account");
		Ih_Apps.setmAccountEndpoint(CloudEndpointUtils.updateBuilder(endpointBuilder).build());
		mAccountEndpoint = Ih_Apps.getmAccountEndpoint();

		AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				Ihmrequestendpoint.Builder requestEndpointBuilder = new Ihmrequestendpoint.Builder(
						AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential);
				requestEndpointBuilder.setApplicationName("InstantHelpers-request");
				Ih_Apps.setmRequestEndpoint(CloudEndpointUtils.updateBuilder(requestEndpointBuilder).build());
				mRequestEndpoint = Ih_Apps.getmRequestEndpoint();

				Ihmorderendpoint.Builder orderEndpointBuilder = new Ihmorderendpoint.Builder(
						AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential);
				orderEndpointBuilder.setApplicationName("InstantHelpers-order");
				Ih_Apps.setmOrderEndpoint(CloudEndpointUtils.updateBuilder(orderEndpointBuilder).build());
				mOrderEndpoint = Ih_Apps.getmOrderEndpoint();

				Ihmquotationendpoint.Builder quotaEndpointBuilder = new Ihmquotationendpoint.Builder(
						AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential);
				quotaEndpointBuilder.setApplicationName("InstantHelpers-quota");
				Ih_Apps.setmQuotationEndpoint(CloudEndpointUtils.updateBuilder(quotaEndpointBuilder).build());
				mQuotationEndpoint = Ih_Apps.getmQuotationEndpoint();
				
				Ihmworkerendpoint.Builder workerEndpointBuilder = new Ihmworkerendpoint.Builder(
						AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential);
				quotaEndpointBuilder.setApplicationName("InstantHelpers-quota");
				Ih_Apps.setmWorkerEndpoint(CloudEndpointUtils.updateBuilder(workerEndpointBuilder).build());
				mWorkerEndpoint = Ih_Apps.getmWorkerEndpoint();

				return null;
			}

		}.execute();

	}

	@Override
	public void onConnectionSuspended(int cause) {
		Log.i(TAG, "onConnectionSuspended");
		mGoogleApiClient.connect();
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.i(TAG, "onStart: mGoogleApiClient.connect()");
		mGoogleApiClient.connect();
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.i(TAG, "onStop():");
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
			Log.i(TAG, "mGoogleApiClient.disconnect()");
		}
	}

	protected void onSignedOut() {
		Log.i(TAG, "onSignedOut() 01: ");

	}

	protected void onSaveIntanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(SAVED_PROGRESS, mSignInProgress);
		Log.e(TAG, "onSaveIntanceState:" + mSignInProgress);
	}

	protected void resolveSignInError() {
		if (mSignInIntent != null) {
			try {
				mSignInProgress = STATE_IN_PROGRESS;
				startIntentSenderForResult(mSignInIntent.getIntentSender(), RC_SIGN_IN, new Intent(), 0, 0, 0);
				Log.i(TAG,
						"resolvSignInError(): startIntentSenderForResult(mSignInIntent.getIntentSender(), RC_SIGN_IN, new Intent(), 0, 0, 0);");
			} catch (SendIntentException e) {
				Log.i(TAG, "Sign in intent could not to send:" + e.getLocalizedMessage());
				mSignInProgress = STATE_SIGN_IN;
				mGoogleApiClient.connect();
			}
		} else {
			showDialog(DIALOG_PLAY_SERVICES_ERROR);
			Log.i(TAG, "resolvSignInError(): showDialog(DIALOG_PLAY_SERVICES_ERROR)");

		}
	}

	protected GoogleApiClient buildGoogleApiClient() {
		return new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this)
				.addApi(Plus.API, Plus.PlusOptions.builder().build()).addScope(Plus.SCOPE_PLUS_LOGIN).build();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_PLAY_SERVICES_ERROR:
			if (GooglePlayServicesUtil.isUserRecoverableError(mSignInError)) {
				return GooglePlayServicesUtil.getErrorDialog(mSignInError, this, RC_SIGN_IN,
						new DialogInterface.OnCancelListener() {
							@Override
							public void onCancel(DialogInterface dialog) {
								Log.e(TAG, "Google Play services resolution cancelled");
								mSignInProgress = STATE_DEFAULT;
							}
						});
			} else {
				return new AlertDialog.Builder(this).setMessage(R.string.play_services_error)
						.setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Log.e(TAG, "Google Play services error could not be resolved: " + mSignInError);
							}
						}).create();
			}
		default:
			return super.onCreateDialog(id);
		}
	}

	protected void showDialog(String message) {
		new AlertDialog.Builder(this).setMessage(message)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				}).show();
	}

}
