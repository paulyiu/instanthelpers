package vtc.project.instanthelpers.fragments;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import vtc.project.instanthelpers.Ih_Apps;
import vtc.project.instanthelpers.R;
import vtc.project.instanthelpers.ihmorderendpoint.Ihmorderendpoint;
import vtc.project.instanthelpers.ihmorderendpoint.model.IhMOrder;
import vtc.project.instanthelpers.ihmquotationendpoint.Ihmquotationendpoint;
import vtc.project.instanthelpers.ihmquotationendpoint.model.IhMQuotation;
import vtc.project.instanthelpers.ihmrequestendpoint.Ihmrequestendpoint;
import vtc.project.instanthelpers.ihmrequestendpoint.model.IhMRequest;
import vtc.project.instanthelpers.util.Ih_QuotaList;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Ih_CustomerMessageFragment extends Fragment {


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View rootView = inflater.inflate(R.layout.fragment_customer_message, container, false);
		final ExpandableListView exList = (ExpandableListView) rootView
				.findViewById(R.id.customer_message_expandablelistview);
		exList.setIndicatorBounds(5, 5);

		AsyncTask<Void, Void, List<IhMRequest>> task = new AsyncTask<Void, Void, List<IhMRequest>>() {
			MessageAdapter exAdpt;
			Ihmrequestendpoint mRequestEndpoint = Ih_Apps.getmRequestEndpoint();
			ProgressDialog dialog;

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = new ProgressDialog(getActivity());
				dialog.setCancelable(false);
				dialog.setTitle("Progress");
				dialog.setMessage("Loading...");
				dialog.show();

			}

			@Override
			protected List<IhMRequest> doInBackground(Void... params) {

				try {
					exAdpt = new MessageAdapter(getActivity(), mRequestEndpoint
							.getIhMRequestByCustomerId(Ih_Apps.getIhmaccount().getId()).execute().getItems());
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(List<IhMRequest> result) {
				super.onPostExecute(result);
				exList.setIndicatorBounds(0, 20);
				exList.setAdapter(exAdpt);
				dialog.dismiss();

			}

		};
		task.execute();
		return rootView;
	}

	private class MessageAdapter extends BaseExpandableListAdapter {
		Context context;
		List<IhMRequest> ihmrequests;
		String[] mOrderType = { "Air", "Fire", "Water", "Electric" };
		int[] mOrderTypeIcon = { R.drawable.air, R.drawable.fire, R.drawable.water, R.drawable.electric };
		boolean[] downloads;
		Bitmap[] bm;
		QuotaAdapter[] qa;
		
		class ViewChildHolder {
			TextView itemTimestamp;
			EditText itemComment;
			EditText itemAddress;
			ImageView itemPhoto;
			Ih_QuotaList itemQuota;
		}

		class ViewGroupHolder {
			TextView groupId;
			ImageView groupType;
		}

		public MessageAdapter(Context context, List<IhMRequest> ihmrequests) {
			this.context = context;
			this.ihmrequests = ihmrequests;
			downloads = new boolean[getGroupCount() + 1];
			bm = new Bitmap[getGroupCount() + 1];
			qa = new QuotaAdapter[getGroupCount() + 1];
		}

		@Override
		public int getGroupCount() {
			return ihmrequests != null ? ihmrequests.size() : 0;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return 1;
		}

		@Override
		public Object getGroup(int groupPosition) {
			return ihmrequests != null ? ihmrequests.get(groupPosition) : null;
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return ihmrequests != null ? ihmrequests.get(groupPosition) : null;
		}

		@Override
		public long getGroupId(int groupPosition) {
			return ihmrequests != null ? ihmrequests.get(groupPosition).hashCode() : null;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return ihmrequests != null ? ihmrequests.get(groupPosition).hashCode() : null;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewGroupHolder vh;
			if (v == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = inflater.inflate(R.layout.listviewgroup_customer_request, parent, false);
				vh = new ViewGroupHolder();
				vh.groupId = (TextView) v.findViewById(R.id.customer_message_group_id);
				vh.groupType = (ImageView) v.findViewById(R.id.customer_message_group_type);
				v.setTag(vh);
			} else {
				vh = (ViewGroupHolder) v.getTag();
			}
			if (ihmrequests != null) {
				IhMRequest ihmrequest = ihmrequests.get(groupPosition);
				vh.groupId.setText("" + ihmrequest.getId());
				vh.groupType.setImageDrawable(getResources().getDrawable(mOrderTypeIcon[ihmrequest.getMOrderType()]));
			}
			return v;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
				ViewGroup parent) {
			View v = convertView;
			ViewChildHolder vh;
			if (v == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = inflater.inflate(R.layout.listviewitem_customer_request, parent, false);
				vh = new ViewChildHolder();
				vh.itemTimestamp = (TextView) v.findViewById(R.id.customer_message_item_timestamp);
				vh.itemComment = (EditText) v.findViewById(R.id.customer_message_item_comment2);
				vh.itemAddress = (EditText) v.findViewById(R.id.customer_message_item_address2);
				vh.itemPhoto = (ImageView) v.findViewById(R.id.customer_message_item_photo);
				vh.itemQuota = (Ih_QuotaList) v.findViewById(R.id.customer_message_quotaion);
				v.setTag(vh);
			} else {
				vh = (ViewChildHolder) v.getTag();
			}
			IhMRequest ihmrequest = ihmrequests.get(groupPosition);

			Calendar cal = Calendar.getInstance(Locale.ENGLISH);
			cal.setTimeInMillis(ihmrequest.getTimestamp());
			String date = DateFormat.format("dd-MM-yyyy", cal).toString();
			vh.itemTimestamp.setText(date);
			vh.itemComment.setText(ihmrequest.getMComment());
			vh.itemAddress.setText(ihmrequest.getMAddress());
			String url = null;
			final float scale = getActivity().getResources().getDisplayMetrics().density;
			if (ihmrequest.getMPhoto() != null) {
				url = "http://instant-helpers.appspot.com/serve?blob-key=" + ihmrequest.getMPhoto().get(0);
				vh.itemPhoto.getLayoutParams().height = (int) (150 * scale + 0.5f);
			} else {
				vh.itemPhoto.getLayoutParams().height = 0;
			}
			String[] args = { url, "" + groupPosition, "" + ihmrequest.getId() };
			vh.itemQuota.setAdapter(qa[groupPosition]);
			if(qa[groupPosition]!=null){
			vh.itemQuota.setRows(qa[groupPosition].getCount(), scale);
			}else{
				vh.itemQuota.setRows(0,0);
			}
			vh.itemPhoto.setImageBitmap(bm[groupPosition]);
			if (!downloads[groupPosition]) {
				new DownloadImageTask(vh.itemPhoto).execute(args);
				new DownloadQuotaTask(ihmrequest, "" + groupPosition, vh.itemQuota).execute();
			}
			return v;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

		private class DownloadQuotaTask extends AsyncTask<Long, String, Ih_QuotaList> {
			private Long id;
			private IhMRequest ihmr;
			private Ihmquotationendpoint endpoint;
			private String message;
			private Ih_QuotaList itemQuota;
			private List<IhMQuotation> list;
			private QuotaAdapter qAdpt;
			ProgressDialog dialog;

			public DownloadQuotaTask(IhMRequest ihmr, String message, Ih_QuotaList itemQuota) {
				this.ihmr = ihmr;
				this.id = ihmr.getId();
				this.message = message;
				this.itemQuota = itemQuota;
				endpoint = Ih_Apps.getmQuotationEndpoint();
			}

			@Override
			protected Ih_QuotaList doInBackground(Long... params) {
				try {
					list = endpoint.getIhMQuotationByRequestId(id).execute().getItems();
					if (list != null && list.size() > 0) {
						qAdpt = new QuotaAdapter(getActivity(), list, ihmr);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				return itemQuota;
			}

			@Override
			protected void onProgressUpdate(String... values) {
				super.onProgressUpdate(values);
				dialog.setMessage(values[0]);
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = new ProgressDialog(getActivity());
				dialog.setCancelable(false);
				dialog.setTitle("Progress");
				dialog.setMessage("Loading quotation...");
				// dialog.show();
			}

			@Override
			protected void onPostExecute(Ih_QuotaList result) {
				super.onPostExecute(result);
				if (qAdpt != null) {
					itemQuota.setAdapter(qAdpt);
					qa[Integer.parseInt(message)] = qAdpt;
					itemQuota.setRows(list.size(),getActivity().getResources().getDisplayMetrics().density);
				} else {
					qa[Integer.parseInt(message)] = null;
					itemQuota.setAdapter(null);
					itemQuota.setRows(0,0);
				}
				dialog.dismiss();
			}

		}

		

		private class QuotaAdapter extends BaseAdapter {
			List<IhMQuotation> ihmquotations;
			Context context;
			IhMRequest ihmr;
			Button[] confirms;

			class ViewQuotaHolder {
				TextView itemHelper;
				TextView itemPrice;
				Button itemConfirm;
				EditText itemRemark;
			}

			public QuotaAdapter(Context context, List<IhMQuotation> ihmquotations, IhMRequest ihmr) {
				this.context = context;
				this.ihmquotations = ihmquotations;
				this.ihmr = ihmr;
				this.confirms = new Button[getCount() + 1];
			}

			@Override
			public int getCount() {
				return ihmquotations != null ? ihmquotations.size() : 0;
			}

			@Override
			public Object getItem(int position) {
				return ihmquotations != null ? ihmquotations.get(position) : null;
			}

			@Override
			public long getItemId(int position) {
				return ihmquotations != null ? ihmquotations.get(position).hashCode() : 0;
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View v = convertView;
				final ViewQuotaHolder vh;

				if (v == null) {
					LayoutInflater inflater = (LayoutInflater) context
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = inflater.inflate(R.layout.listviewitem_customer_request_quota, parent, false);
					vh = new ViewQuotaHolder();
					vh.itemHelper = (TextView) v.findViewById(R.id.customer_message_quota_helper);
					vh.itemPrice = (TextView) v.findViewById(R.id.customer_message_quota_price);
					vh.itemConfirm = (Button) v.findViewById(R.id.customer_message_quota_confirm);
					vh.itemRemark = (EditText) v.findViewById(R.id.customer_message_quota_remark);
					v.setTag(vh);
				} else {
					vh = (ViewQuotaHolder) v.getTag();
				}

				final IhMQuotation ihmq = ihmquotations.get(position);

				vh.itemHelper.setText("" + ihmq.getMHelperId());
				vh.itemPrice.setText("$" + ihmq.getMPrice());
				if (ihmq.getMRemark() != null)
					vh.itemRemark.setText("" + ihmq.getMRemark());
				confirms[position] = vh.itemConfirm;

				if (ihmr.getMOrderId() == null) {
					vh.itemConfirm.setTag(ihmq.getId());
					vh.itemConfirm.setOnClickListener(new OnClickListener() {
						public void onClick(final View v) {
							Long mQuotationId = ihmq.getId();
							Long mHelperId = ihmq.getMHelperId();
							AsyncTask<Long, String, IhMOrder> task = new AsyncTask<Long, String, IhMOrder>() {
								Ihmorderendpoint ihmoEndpoint = Ih_Apps.getmOrderEndpoint();
								Ihmrequestendpoint ihmrEndpoint = Ih_Apps.getmRequestEndpoint();
								@Override
								protected IhMOrder doInBackground(Long... params) {
									IhMOrder ihmo = null;
									if (params.length >= 2) {
										IhMOrder content = new IhMOrder();
										content.setMQuotationId(params[0]);
										content.setMHelperId(params[1]);
										content.setMRequestId(ihmr.getId());
										try {
											ihmo = ihmoEndpoint.insertIhMOrder(content).execute();
											ihmr.setMOrderId(ihmo.getId());
											ihmr.setMQuotationId(ihmq.getId());
											ihmrEndpoint.updateIhMRequest(ihmr).execute();
										} catch (IOException e) {
											e.printStackTrace();
										}
									}
									return ihmo;
								}

								@Override
								protected void onPostExecute(IhMOrder result) {
									super.onPostExecute(result);
									((View) v.getParent().getParent()).setBackgroundColor(Color.parseColor("#7FF771"));
									for (Button b : confirms) {
										if (b != null) {
											b.setVisibility(View.GONE);
										}
									}
								}

							};
							task.execute(mQuotationId, mHelperId);

						}

					});
				} else {
					vh.itemConfirm.setVisibility(View.GONE);
					if(ihmr.getMQuotationId().equals(ihmq.getId())){
						v.setBackgroundColor(Color.parseColor("#7FF771"));
					}
				}
				return v;
			}

		}

		private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
			ImageView bmImage;
			ProgressDialog dialog;
			int groupPosition;

			public DownloadImageTask(ImageView bmImage) {
				this.bmImage = bmImage;
			}

			protected Bitmap doInBackground(String... urls) {
				String urldisplay = urls[0];
				groupPosition = Integer.parseInt(urls[1]);
				Bitmap mIcon11 = null;
				if (urldisplay != null) {
					try {
						InputStream in = new java.net.URL(urldisplay).openStream();
						mIcon11 = BitmapFactory.decodeStream(in);
					} catch (Exception e) {
						Log.e("Error", e.getMessage());
						e.printStackTrace();
					}
				}
				return mIcon11;
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = new ProgressDialog(getActivity());
				dialog.setCancelable(false);
				dialog.setTitle("Progress");
				dialog.setMessage("Downloading photo...");
				// dialog.show();

			}

			protected void onPostExecute(Bitmap result) {
				bmImage.setImageBitmap(result);
				bm[groupPosition] = result;
				downloads[groupPosition] = true;
				dialog.dismiss();
			}
		}
	}

}
