package vtc.project.instanthelpers.fragments;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import vtc.project.instanthelpers.R;
import vtc.project.instanthelpers.activitys.Ih_CustomerDrawerActivity;
import vtc.project.instanthelpers.ihmrequestendpoint.model.IhMRequest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class Ih_CustomerHomeFragment extends Fragment implements
		OnClickListener {
	private ImageView wvNews;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	private ViewFlipper mViewFlipper;
	private Context mContext;
	private IhMRequest ihmrequest;

	private final GestureDetector detector = new GestureDetector(
			new SwipeGestureDetector());

	public Ih_CustomerHomeFragment() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		View rootView = inflater.inflate(R.layout.fragment_customer_home,
				container, false);
		mContext = getActivity();
		mViewFlipper = (ViewFlipper) rootView.findViewById(R.id.view_flipper);
		mViewFlipper.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(final View view, final MotionEvent event) {
				detector.onTouchEvent(event);
				return true;
			}
		});
		mViewFlipper.setAutoStart(true);
		mViewFlipper.setFlipInterval(3000);
		mViewFlipper.startFlipping();

		rootView.findViewById(R.id.button_air).setOnClickListener(this);
		rootView.findViewById(R.id.button_fire).setOnClickListener(this);
		rootView.findViewById(R.id.button_water).setOnClickListener(this);
		rootView.findViewById(R.id.button_electric).setOnClickListener(this);
		rootView.findViewById(R.id.desc_air).setOnClickListener(this);
		rootView.findViewById(R.id.desc_fire).setOnClickListener(this);
		rootView.findViewById(R.id.desc_water).setOnClickListener(this);
		rootView.findViewById(R.id.desc_electric).setOnClickListener(this);

		TextView textView = (TextView) rootView.findViewById(R.id.desc_first);
		textView.setText(Html.fromHtml(getString(R.string.desc_first)));
		textView = (TextView) rootView.findViewById(R.id.desc_second);
		textView.setText(Html.fromHtml(getString(R.string.desc_second)));
		textView = (TextView) rootView.findViewById(R.id.desc_third);
		textView.setText(Html.fromHtml(getString(R.string.desc_third)));
		System.out.println("start load webview");
		wvNews = (ImageView) rootView.findViewById(R.id.wvNews);

		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(
					"http://demo2056992.mockable.io/uriList");
			HttpResponse response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			StringBuffer reply = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				reply.append(line);
			}

			JSONObject json = new JSONObject(reply.toString());
			JSONArray codeArray = json.getJSONArray("UriList");
			int r = (int) (Math.random() * codeArray.length());
			String url = codeArray.getJSONObject(r).getString("uri");
			if (wvNews.getTag() != "Dowloaded") {
				new DownloadImageTask(wvNews).execute(url);
				System.out.println("load url" + url);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" load webview error" + e.getMessage());
			System.out.println("end load webview");
		}
		ListView mDrawerList = (ListView) ((Ih_CustomerDrawerActivity) getActivity())
				.findViewById(R.id.left_drawer);
		int position = 0;
		mDrawerList.setItemChecked(position, true);
		mDrawerList.setSelection(position);

		return rootView;
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
			bmImage.setTag("Downloaded");
		}
	}

	class SwipeGestureDetector extends SimpleOnGestureListener {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			try {
				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					mViewFlipper.stopFlipping();
					mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(
							mContext, R.anim.slide_in_left));
					mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(
							mContext, R.anim.slide_out_left));
					mViewFlipper.showNext();
					mViewFlipper.setFlipInterval(3000);
					mViewFlipper.startFlipping();
					return true;
				} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					mViewFlipper.stopFlipping();
					mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(
							mContext, R.anim.slide_in_right));
					mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(
							mContext, R.anim.slide_out_right));
					mViewFlipper.showNext();
					mViewFlipper.setFlipInterval(3000);
					mViewFlipper.startFlipping();
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		}
	}

	@Override
	public void onClick(View v) {
		Fragment fragment = null;
		Bundle bundle = new Bundle();
		fragment = new Ih_CustomerOrderFragment();
		switch (v.getId()) {
		case R.id.desc_air:
		case R.id.button_air:
			bundle.putString("OrderType", "Air");
			break;
		case R.id.desc_fire:
		case R.id.button_fire:
			bundle.putString("OrderType", "Fire");
			break;
		case R.id.desc_water:
		case R.id.button_water:
			bundle.putString("OrderType", "Water");
			break;
		case R.id.desc_electric:
		case R.id.button_electric:
			bundle.putString("OrderType", "Electric");
			break;
		}
		fragment.setArguments(bundle);
		if (fragment != null) {
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.content_frame, fragment)
					.addToBackStack(fragment.getClass().getName()).commit();

		}
	}
}
