package vtc.project.instanthelpers.fragments;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import vtc.project.instanthelpers.R;
import vtc.project.instanthelpers.util.Ih_DownloadImageTask;

public class Ih_CustomerContactFragment extends Fragment implements OnClickListener {
	private ImageView wvNews;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	private ViewFlipper mViewFlipper;
	private Context mContext;
	LinearLayout panelBackgroud, panelContact;
	TextView tvBackgroud, tvContactUs;
	EditText contact_etSubject, contact_message_data;
	Button btContactSubmit;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View rootView = inflater.inflate(R.layout.fragment_customer_contact, container, false);
		mContext = getActivity();
		System.out.println("start load webview");

		wvNews = (ImageView) rootView.findViewById(R.id.wvNews);
		System.out.println("start load webview");
		wvNews = (ImageView) rootView.findViewById(R.id.wvNews);
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet("http://demo2056992.mockable.io/uriList");
			HttpResponse response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer reply = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				reply.append(line);
			}

			JSONObject json = new JSONObject(reply.toString());
			JSONArray codeArray = json.getJSONArray("UriList");
			int r = (int) (Math.random() * codeArray.length());
			String url = codeArray.getJSONObject(r).getString("uri");
			if (wvNews.getTag() != "Dowloaded") {
				new Ih_DownloadImageTask(mContext, wvNews, false).execute(url);
				System.out.println("load url" + url);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" load webview error" + e.getMessage());
			System.out.println("end load webview");
		}
		System.out.println("end load webview");

		panelBackgroud = (LinearLayout) rootView.findViewById(R.id.contact_panel_backgroud);
		panelContact = (LinearLayout) rootView.findViewById(R.id.contact_panel_contact);
		panelBackgroud.setVisibility(View.VISIBLE);
		panelContact.setVisibility(View.GONE);
		tvBackgroud = (TextView) rootView.findViewById(R.id.contact_title_backgroud);
		tvContactUs = (TextView) rootView.findViewById(R.id.contact_title_contact);
		contact_etSubject = (EditText) rootView.findViewById(R.id.contact_etSubject);
		contact_message_data = (EditText) rootView.findViewById(R.id.contact_message_data);
		btContactSubmit = (Button) rootView.findViewById(R.id.contact_submit);
		btContactSubmit.setOnClickListener(this);
		tvBackgroud.setOnClickListener(this);
		tvContactUs.setOnClickListener(this);

		return rootView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.contact_title_backgroud:
		case R.id.contact_title_contact:
			panelBackgroud.setVisibility(panelBackgroud.isShown() ? View.GONE : View.VISIBLE);
			panelContact.setVisibility(panelContact.isShown() ? View.GONE : View.VISIBLE);
			break;
		case R.id.contact_submit:
			sendEmail();
			break;
		}

	}

	private void sendEmail() {
		String[] recipients = { "paul@metis.hk" };
		Intent email = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
		// prompts email clients only
		email.setType("message/rfc822");

		email.putExtra(Intent.EXTRA_EMAIL, recipients);
		email.putExtra(Intent.EXTRA_SUBJECT, contact_etSubject.getText().toString());
		email.putExtra(Intent.EXTRA_TEXT, contact_message_data.getText().toString());

		try {
			// the user can choose the email client
			startActivity(Intent.createChooser(email, "Choose an email client from..."));
			contact_etSubject.setText("");
			contact_message_data.setText("");
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(getActivity(), "No email client installed.", Toast.LENGTH_LONG).show();
		}

	}
}
