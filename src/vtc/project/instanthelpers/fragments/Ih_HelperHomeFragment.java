package vtc.project.instanthelpers.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.text.Layout;
import android.text.format.DateFormat;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import vtc.project.instanthelpers.Ih_Apps;
import vtc.project.instanthelpers.R;
import vtc.project.instanthelpers.ihmquotationendpoint.Ihmquotationendpoint;
import vtc.project.instanthelpers.ihmquotationendpoint.model.IhMQuotation;
import vtc.project.instanthelpers.ihmrequestendpoint.Ihmrequestendpoint;
import vtc.project.instanthelpers.ihmrequestendpoint.model.IhMRequest;

public class Ih_HelperHomeFragment extends Fragment {
	private ImageView wvNews;

	public Ih_HelperHomeFragment() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		View rootView = inflater.inflate(R.layout.fragment_helper_home, container, false);
		final ExpandableListView exList = (ExpandableListView) rootView
				.findViewById(R.id.helper_message_expandablelistview);
		exList.setIndicatorBounds(5, 5);
		System.out.println("start load webview");
		wvNews = (ImageView) rootView.findViewById(R.id.wvNews);
		TextView title = (TextView)rootView.findViewById(R.id.helper_title);
		title.setText("New Request");
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet("http://demo2056992.mockable.io/uriList");
			HttpResponse response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer reply = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				reply.append(line);
			}

			JSONObject json = new JSONObject(reply.toString());
			JSONArray codeArray = json.getJSONArray("UriList");
			int r = (int) (Math.random() * codeArray.length());
			String url = codeArray.getJSONObject(r).getString("uri");
			if (wvNews.getTag() != "Dowloaded") {
				new DownloadImageTask(wvNews).execute(url);
				System.out.println("load url" + url);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" load webview error" + e.getMessage());
			System.out.println("end load webview");
		}
		System.out.println("end load webview");
		AsyncTask<Void, Void, List<IhMRequest>> task = new AsyncTask<Void, Void, List<IhMRequest>>() {
			MessageAdapter exAdpt;
			Ihmrequestendpoint mRequestEndpoint = Ih_Apps.getmRequestEndpoint();
			ProgressDialog dialog;

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = new ProgressDialog(getActivity());
				dialog.setCancelable(false);
				dialog.setTitle("Progress");
				dialog.setMessage("Loading...");
				dialog.show();

			}

			@Override
			protected List<IhMRequest> doInBackground(Void... params) {

				try {
					exAdpt = new MessageAdapter(getActivity(),
							mRequestEndpoint.getIhMRequestNotConfirmed().execute().getItems());
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(List<IhMRequest> result) {
				super.onPostExecute(result);
				exList.setIndicatorBounds(0, 20);
				exList.setAdapter(exAdpt);
				dialog.dismiss();

			}

		};
		task.execute();
		return rootView;
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
			bmImage.setTag("Downloaded");
		}
	}

	private class MessageAdapter extends BaseExpandableListAdapter {
		Context context;
		List<IhMRequest> ihmrequests;
		String[] mOrderType = { "Air", "Fire", "Water", "Electric" };
		String[] mRegion = { "Hong Kong", "Kowloon", "New Territories" };
		int[] mOrderTypeIcon = { R.drawable.air, R.drawable.fire, R.drawable.water, R.drawable.electric };
		boolean[] downloads;
		IhMQuotation[] qa;
		Bitmap[] bm;

		class ViewChildHolder {
			TextView itemTimestamp;
			EditText itemComment;
			EditText itemAddress;
			ImageView itemPhoto;
			Button itemQuota;
		}

		class ViewGroupHolder {
			TextView groupId;
			ImageView groupType;
		}

		public MessageAdapter(Context context, List<IhMRequest> ihmrequests) {
			this.context = context;
			this.ihmrequests = ihmrequests;
			downloads = new boolean[getGroupCount() + 1];
			qa = new IhMQuotation[getGroupCount() + 1];
			bm = new Bitmap[getGroupCount() + 1];
		}

		@Override
		public int getGroupCount() {
			return ihmrequests != null ? ihmrequests.size() : 0;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return 1;
		}

		@Override
		public Object getGroup(int groupPosition) {
			return ihmrequests != null ? ihmrequests.get(groupPosition) : null;
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return ihmrequests != null ? ihmrequests.get(groupPosition) : null;
		}

		@Override
		public long getGroupId(int groupPosition) {
			return ihmrequests != null ? ihmrequests.get(groupPosition).hashCode() : null;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return ihmrequests != null ? ihmrequests.get(groupPosition).hashCode() : null;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewGroupHolder vh;
			if (v == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = inflater.inflate(R.layout.listviewgroup_customer_request, parent, false);
				vh = new ViewGroupHolder();
				vh.groupId = (TextView) v.findViewById(R.id.customer_message_group_id);
				vh.groupType = (ImageView) v.findViewById(R.id.customer_message_group_type);
				v.setTag(vh);
			} else {
				vh = (ViewGroupHolder) v.getTag();
			}
			if (ihmrequests != null) {
				IhMRequest ihmrequest = ihmrequests.get(groupPosition);
				vh.groupId.setText("" + ihmrequest.getId());
				vh.groupType.setImageDrawable(getResources().getDrawable(mOrderTypeIcon[ihmrequest.getMOrderType()]));
			}
			return v;
		}

		@Override
		public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView,
				ViewGroup parent) {
			View v = convertView;
			final ViewChildHolder vh;
			final IhMRequest ihmrequest = ihmrequests.get(groupPosition);
			if (v == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = inflater.inflate(R.layout.listviewitem_helper_request, parent, false);
				vh = new ViewChildHolder();
				vh.itemTimestamp = (TextView) v.findViewById(R.id.helper_message_item_timestamp);
				vh.itemComment = (EditText) v.findViewById(R.id.helper_message_item_comment2);
				vh.itemAddress = (EditText) v.findViewById(R.id.helper_message_item_address2);
				vh.itemPhoto = (ImageView) v.findViewById(R.id.helper_message_item_photo);
				vh.itemQuota = (Button) v.findViewById(R.id.helper_message_quotaion);
				v.setTag(vh);
			} else {
				vh = (ViewChildHolder) v.getTag();
			}
			vh.itemQuota.setText("" + ihmrequest.getId());
			final Dialog dialog = new Dialog(context);
			dialog.setTitle("Quotation:");
			dialog.setCancelable(false);
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			LinearLayout layout = new LinearLayout(context);
			layout.setLayoutParams(params);
			layout.setOrientation(LinearLayout.VERTICAL);
			TextView pricelabel = new TextView(context);
			pricelabel.setText("Price:");
			pricelabel.setLayoutParams(params);
			TextView remarklabel = new TextView(context);
			remarklabel.setText("Remark:");
			remarklabel.setLayoutParams(params);
			final EditText price = new EditText(context);
			price.setLayoutParams(params);
			price.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
			price.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
			final EditText remark = new EditText(context);
			remark.setLayoutParams(params);
			final OnClickListener submitQuota = new OnClickListener() {
				@Override
				public void onClick(final View v) {
					final Long mRequestId = ihmrequest.getId();
					final Long mHelperId = Ih_Apps.getIhmaccount().getId();
					final String mPrice = price.getText().toString();
					final String mRemark = remark.getText().toString();
					AsyncTask<Object, String, IhMQuotation> task = new AsyncTask<Object, String, IhMQuotation>() {
						Ihmquotationendpoint ihmqEndpoint = Ih_Apps.getmQuotationEndpoint();
						Button button;

						@Override
						protected IhMQuotation doInBackground(Object... params) {
							button = (Button) params[0];
							IhMQuotation ihmq = null;
							if (mPrice != null && mPrice.length() > 0) {
								IhMQuotation content = new IhMQuotation();
								content.setMRequestId(mRequestId);
								content.setMHelperId(mHelperId);
								content.setMPrice(Integer.parseInt(mPrice));
								content.setMRemark(mRemark);
								try {
									ihmq = ihmqEndpoint.insertIhMQuotation(content).execute();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
							return ihmq;
						}

						@Override
						protected void onPostExecute(IhMQuotation result) {
							super.onPostExecute(result);
							dialog.dismiss();
							if (result != null) {
								button.setText("$" + result.getMPrice());
								button.setEnabled(false);
							}
							qa[groupPosition] = result;
						}
					};
					task.execute(vh.itemQuota);
				}
			};

			Button submit = new Button(context);
			submit.setText("Send");
			submit.setLayoutParams(params);
			submit.setOnClickListener(submitQuota);
			Button cancel = new Button(context);
			cancel.setLayoutParams(params);
			cancel.setText("Cancel");
			cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			layout.addView(pricelabel);
			layout.addView(price);
			layout.addView(remarklabel);
			layout.addView(remark);
			layout.addView(submit);
			layout.addView(cancel);
			dialog.setContentView(layout, params);

			OnClickListener newQuotaClick = new OnClickListener() {
				public void onClick(View v) {

					dialog.show();
				}

			};

			vh.itemQuota.setOnClickListener(newQuotaClick);
			Calendar cal = Calendar.getInstance(Locale.ENGLISH);
			cal.setTimeInMillis(ihmrequest.getTimestamp());
			String date = DateFormat.format("dd-MM-yyyy", cal).toString();
			vh.itemTimestamp.setText(date);
			vh.itemComment.setText(ihmrequest.getMComment());
			vh.itemAddress.setText(mRegion[Integer.parseInt(ihmrequest.getMRegion())]);
			String url = null;
			final float scale = getActivity().getResources().getDisplayMetrics().density;
			if (ihmrequest.getMPhoto() != null) {
				url = "http://instant-helpers.appspot.com/serve?blob-key=" + ihmrequest.getMPhoto().get(0);
				vh.itemPhoto.getLayoutParams().height = (int) (150 * scale + 0.5f);
			} else {
				vh.itemPhoto.getLayoutParams().height = 0;
			}

			String[] args = { url, "" + groupPosition, "" + ihmrequest.getId() };

			vh.itemPhoto.setImageBitmap(bm[groupPosition]);
			vh.itemQuota.setVisibility(View.GONE);
			vh.itemQuota.setEnabled(false);
			if (!downloads[groupPosition]) {
				new DownloadImageTask(vh.itemPhoto).execute(args);
				AsyncTask<Long, String, List<IhMQuotation>> task = new AsyncTask<Long, String, List<IhMQuotation>>() {
					Ihmquotationendpoint ihmqEndpoint = Ih_Apps.getmQuotationEndpoint();
					int position;

					@Override
					protected List<IhMQuotation> doInBackground(Long... params) {
						List<IhMQuotation> ihmq = null;
						position = params[2].intValue();
						try {
							ihmq = ihmqEndpoint.getIhMQuotationByRequestAndHelperId(params[0], params[1]).execute()
									.getItems();
						} catch (IOException e) {
							e.printStackTrace();
						}
						return ihmq;
					}

					@Override
					protected void onPostExecute(List<IhMQuotation> result) {
						super.onPostExecute(result);
						if (result != null) {
							qa[position] = result.get(0);
							vh.itemQuota.setEnabled(false);
							vh.itemQuota.setText("$" + qa[position].getMPrice().toString());
							vh.itemQuota.setVisibility(View.VISIBLE);
						} else {
							vh.itemQuota.setEnabled(true);
							vh.itemQuota.setText("Quotation");
							vh.itemQuota.setVisibility(View.VISIBLE);

						}
					}

				};
				task.execute(Ih_Apps.getIhmaccount().getId(), ihmrequest.getId(), Long.parseLong("" + groupPosition));
			} else {
				if (qa[groupPosition] != null) {
					vh.itemQuota.setEnabled(false);
					vh.itemQuota.setText("$" + qa[groupPosition].getMPrice().toString());
					vh.itemQuota.setVisibility(View.VISIBLE);
				} else {
					vh.itemQuota.setEnabled(true);
					vh.itemQuota.setText("Quotation");
					vh.itemQuota.setVisibility(View.VISIBLE);

				}
			}

			return v;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

		private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
			ImageView bmImage;
			ProgressDialog dialog;
			int groupPosition;

			public DownloadImageTask(ImageView bmImage) {
				this.bmImage = bmImage;
			}

			protected Bitmap doInBackground(String... urls) {
				String urldisplay = urls[0];
				groupPosition = Integer.parseInt(urls[1]);
				Bitmap mIcon11 = null;
				if (urldisplay != null) {
					try {
						InputStream in = new java.net.URL(urldisplay).openStream();
						mIcon11 = BitmapFactory.decodeStream(in);
					} catch (Exception e) {
						Log.e("Error", e.getMessage());
						e.printStackTrace();
					}
				}
				return mIcon11;
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = new ProgressDialog(getActivity());
				dialog.setCancelable(false);
				dialog.setTitle("Progress");
				dialog.setMessage("Downloading photo...");
				// dialog.show();

			}

			protected void onPostExecute(Bitmap result) {
				bmImage.setImageBitmap(result);
				bm[groupPosition] = result;
				downloads[groupPosition] = true;
				dialog.dismiss();
			}
		}
	}

}
