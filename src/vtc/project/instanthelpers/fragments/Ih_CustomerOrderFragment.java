package vtc.project.instanthelpers.fragments;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import vtc.project.instanthelpers.Ih_Apps;
import vtc.project.instanthelpers.R;
import vtc.project.instanthelpers.activitys.Ih_CustomerDrawerActivity;
import vtc.project.instanthelpers.ihmaccountendpoint.model.IhMAccount;
import vtc.project.instanthelpers.ihmrequestendpoint.Ihmrequestendpoint;
import vtc.project.instanthelpers.ihmrequestendpoint.model.IhMRequest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Ih_CustomerOrderFragment extends Fragment {

	protected static final int SELECT_FILE = 1;
	protected static final int REQUEST_CAMERA = 0;
	private Button order_btn_upload, order_btn_submit;
	private ImageView order_img_upload;
	private Spinner order_spinner_type;
	private Bitmap bm;

	public Ih_CustomerOrderFragment() {

	}

	private OnClickListener mSelectImageOnClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			selectImage();
		}
	};

	private OnClickListener mSubmitRequestOnClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			submitRequest();
		}

	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		View rootView = inflater.inflate(R.layout.fragment_customer_order, container, false);
		order_btn_upload = (Button) rootView.findViewById(R.id.order_btn_upload);
		order_btn_upload.setOnClickListener(mSelectImageOnClick);
		order_btn_submit = (Button) rootView.findViewById(R.id.order_submit);
		order_btn_submit.setOnClickListener(mSubmitRequestOnClick);
		order_img_upload = (ImageView) rootView.findViewById(R.id.order_img_upload);

		order_spinner_type = (Spinner) rootView.findViewById(R.id.order_data_type);
		if (getArguments() != null) {
			switch (getArguments().getString("OrderType")) {
			case "Air":
				order_spinner_type.setSelection(0);
				break;
			case "Fire":
				order_spinner_type.setSelection(1);
				break;
			case "Water":
				order_spinner_type.setSelection(2);
				break;
			case "Electric":
				order_spinner_type.setSelection(3);
				break;
			}
		}
		ListView mDrawerList = (ListView) ((Ih_CustomerDrawerActivity) getActivity()).findViewById(R.id.left_drawer);
		int position = 1;
		mDrawerList.setItemChecked(position, true);
		mDrawerList.setSelection(position);
		return rootView;
	}

	private void submitRequest() {
		IhMAccount ihmaccount = Ih_Apps.getIhmaccount();
		final IhMRequest ihmrequest = new IhMRequest();
		Integer mOrderType = ((Spinner) getActivity().findViewById(
				R.id.order_data_type)).getSelectedItemPosition();
		String mContractNumber = ((TextView) getActivity().findViewById(
				R.id.order_data_phone)).getText().toString();
		String mRegion = ""
				+ ((Spinner) getActivity().findViewById(R.id.order_data_region))
						.getSelectedItemPosition();
		String mAddress = ((TextView) getActivity().findViewById(
				R.id.order_data_address)).getText().toString();
		String mComment = ((TextView) getActivity().findViewById(
				R.id.order_data_comment)).getText().toString();

		ihmrequest.setMOrderType(mOrderType);
		ihmrequest.setMCustomerId(ihmaccount.getId());
		ihmrequest.setMContractNumber(mContractNumber);
		ihmrequest.setMRegion(mRegion);
		ihmrequest.setMAddress(mAddress);
		ihmrequest.setMComment(mComment);
		final Ihmrequestendpoint mRequestEndpoint = Ih_Apps
				.getmRequestEndpoint();
		AsyncTask<Void, String, IhMRequest> task = new AsyncTask<Void, String, IhMRequest>() {
			IhMRequest ihmr;
			ProgressDialog dialog;

			@Override
			protected void onPostExecute(IhMRequest result) {
				super.onPostExecute(result);
				FragmentManager fragmentManager = getFragmentManager();
				Fragment fragment = new Ih_CustomerOrderFragment();
				fragmentManager.beginTransaction()
						.replace(R.id.content_frame, fragment)
						.addToBackStack(fragment.getClass().getName()).commit();
				ListView mDrawerList = (ListView) ((Ih_CustomerDrawerActivity) getActivity())
						.findViewById(R.id.left_drawer);
				int position = 1;
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);
				((Spinner) getActivity().findViewById(R.id.order_data_type)).setSelection(0);
				((TextView) getActivity().findViewById(R.id.order_data_phone)).setText("");
				((Spinner) getActivity().findViewById(R.id.order_data_region)).setSelection(0);
				((TextView) getActivity().findViewById(R.id.order_data_address)).setText("");
				((TextView) getActivity().findViewById(R.id.order_data_comment)).setText("");
				
				dialog.dismiss();
				Toast.makeText(getActivity(), "Request sent: " + ihmr.getId(),
						Toast.LENGTH_LONG).show();

			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = new ProgressDialog(getActivity());
				dialog.setCancelable(false);
				dialog.setTitle("Progress");
				dialog.setMessage("Submitting request...");
				dialog.show();
			}

			@Override
			protected void onProgressUpdate(String... values) {
				super.onProgressUpdate(values);
				dialog.setMessage(values[0]+"..."); 

			}

			@Override
			protected IhMRequest doInBackground(Void... params) {
				try {
					ihmr = mRequestEndpoint.insertIhMRequest(ihmrequest)
							.execute();
					publishProgress("creating request");
					List<String> mPhone = new ArrayList<String>();
					if (bm != null) {
						Log.i("submitRequest:", "" + bm.getByteCount());
						try {

							HttpClient httpClient = new DefaultHttpClient();
							HttpResponse response;
							BufferedReader reader;
							String sResponse;
							StringBuilder s;
							response = httpClient
									.execute(new HttpGet(
											"http://instant-helpers.appspot.com/blob.jsp"));
							publishProgress("requesting upload path");
							reader = new BufferedReader(new InputStreamReader(
									response.getEntity().getContent(), "UTF-8"));
							s = new StringBuilder();
							while ((sResponse = reader.readLine()) != null) {
								s = s.append(sResponse);
							}
							Log.i("submitRequest", "Post Url:" + s.toString());
							HttpPost postRequest = new HttpPost(s.toString());
							ByteArrayOutputStream bos = new ByteArrayOutputStream();
							bm.compress(CompressFormat.JPEG, 75, bos);
							byte[] data = bos.toByteArray();
							// ByteArrayBody bab = new ByteArrayBody(data,
							// "image.jpg");
							// MultipartEntity reqEntity = new
							// MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
							// reqEntity.addPart("blobImg", bab);
							// postRequest.addHeader("Content-Type",
							// "image/jpeg");
							MultipartEntityBuilder reqEntityBuilder = MultipartEntityBuilder
									.create();
							reqEntityBuilder.addBinaryBody("blobImg", data,
									ContentType.create("image/jpeg"),
									ihmr.getMCustomerId() + "-" + ihmr.getId()
											+ ".jpg");
							HttpEntity reqEntity = reqEntityBuilder.build();
							postRequest.setEntity(reqEntity);
							publishProgress("starting photo upload");
							response = httpClient.execute(postRequest);

							reader = new BufferedReader(new InputStreamReader(
									response.getEntity().getContent(), "UTF-8"));

							s = new StringBuilder();
							while ((sResponse = reader.readLine()) != null) {
								s = s.append(sResponse);
							}
							Log.i("submitRequest:", s.toString());
							mPhone.add(s.toString());
						} catch (IllegalStateException | IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						ihmr.setMPhoto(mPhone);

						publishProgress("updating request");
						mRequestEndpoint.updateIhMRequest(ihmr).execute();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}
		};
		task.execute();

	}

	private void selectImage() {
		final CharSequence[] items = { "Take Photo", "Choose from Library", "Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
		builder.setTitle("Add Photo");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (items[item].equals("Take Photo")) {
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
					startActivityForResult(intent, REQUEST_CAMERA);
				} else if (items[item].equals("Choose from Library")) {
					Intent intent = new Intent(Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
				} else if (items[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == REQUEST_CAMERA) {
				File f = new File(Environment.getExternalStorageDirectory().toString());
				for (File temp : f.listFiles()) {
					if (temp.getName().equals("temp.jpg")) {
						f = temp;
						break;
					}
				}
				try {
					BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
					bitmapOptions.inSampleSize = 2;
					bm = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
					order_img_upload.setImageBitmap(bm);
					String path = android.os.Environment.getExternalStorageDirectory() + File.separator + "Phoenix"
							+ File.separator + "default";
					f.delete();
					OutputStream fOut = null;
					File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
					try {
						fOut = new FileOutputStream(file);
						bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
						fOut.flush();
						fOut.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (requestCode == SELECT_FILE) {
				Uri selectedImageUri = data.getData();
				String tempPath = getPath(selectedImageUri, getActivity());
				BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
				bitmapOptions.inSampleSize = 2;
				bm = BitmapFactory.decodeFile(tempPath, bitmapOptions);
				order_img_upload.setImageBitmap(bm);

			}
		}
	}

	private String getPath(Uri uri, Activity activity) {
		String[] projection = { MediaColumns.DATA };
		Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
}
