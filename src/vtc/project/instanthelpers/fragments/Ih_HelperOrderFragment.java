package vtc.project.instanthelpers.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import vtc.project.instanthelpers.Ih_Apps;
import vtc.project.instanthelpers.R;
import vtc.project.instanthelpers.ihmorderendpoint.Ihmorderendpoint;
import vtc.project.instanthelpers.ihmorderendpoint.model.IhMOrder;
import vtc.project.instanthelpers.ihmrequestendpoint.Ihmrequestendpoint;
import vtc.project.instanthelpers.ihmrequestendpoint.model.IhMRequest;
import vtc.project.instanthelpers.ihmworkerendpoint.Ihmworkerendpoint;
import vtc.project.instanthelpers.ihmworkerendpoint.model.IhMWorker;
import vtc.project.instanthelpers.util.Ih_DownloadImageTask;
import vtc.project.instanthelpers.util.Ih_MessageAdapter;
import vtc.project.instanthelpers.util.Ih_Tools;

public class Ih_HelperOrderFragment extends Fragment {
	private ImageView wvNews;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	private ViewFlipper mViewFlipper;
	private Context mContext;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View rootView = inflater.inflate(R.layout.fragment_helper_order, container, false);
		mContext = getActivity();
		System.out.println("start load webview");
		final ExpandableListView exList = (ExpandableListView) rootView
				.findViewById(R.id.helper_order_expandablelistview);
		exList.setIndicatorBounds(5, 5);
		wvNews = (ImageView) rootView.findViewById(R.id.wvNews);
		System.out.println("start load webview");
		wvNews = (ImageView) rootView.findViewById(R.id.wvNews);
		TextView title = (TextView) rootView.findViewById(R.id.helper_title);
		title.setText("New Order");
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet("http://demo2056992.mockable.io/uriList");
			HttpResponse response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer reply = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				reply.append(line);
			}

			JSONObject json = new JSONObject(reply.toString());
			JSONArray codeArray = json.getJSONArray("UriList");
			int r = (int) (Math.random() * codeArray.length());
			String url = codeArray.getJSONObject(r).getString("uri");
			if (wvNews.getTag() != "Dowloaded") {
				new Ih_DownloadImageTask(mContext, wvNews, false).execute(url);
				System.out.println("load url" + url);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" load webview error" + e.getMessage());
			System.out.println("end load webview");
		}
		System.out.println("end load webview");

		AsyncTask<Void, Void, List<IhMRequest>> task = new AsyncTask<Void, Void, List<IhMRequest>>() {
			Ih_MessageAdapter exAdpt;
			Ihmrequestendpoint mRequestEndpoint = Ih_Apps.getmRequestEndpoint();
			Ihmorderendpoint mOrderEndpoint = Ih_Apps.getmOrderEndpoint();
			Ihmworkerendpoint mWorkerEndpoint = Ih_Apps.getmWorkerEndpoint();
			List<IhMOrder> ihmos;
			List<IhMWorker> ihmws;
			List<String> ihmwids;
			List<IhMRequest> ihmrs;
			ProgressDialog dialog;

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = new ProgressDialog(getActivity());
				dialog.setCancelable(false);
				dialog.setTitle("Progress");
				dialog.setMessage("Loading...");
				dialog.show();

			}

			@Override
			protected List<IhMRequest> doInBackground(Void... params) {

				try {
					ihmws = mWorkerEndpoint.getIhMWorkerByHelperId(Ih_Apps.getIhmaccount().getId()).execute()
							.getItems();
					ihmos = mOrderEndpoint.getIhMOrderByHelperIdNew(Ih_Apps.getIhmaccount().getId()).execute()
							.getItems();
					if (ihmws != null) {
						ihmwids = new ArrayList<String>();
						for (IhMWorker w : ihmws) {
							ihmwids.add("" + w.getId());
						}
					}
					if (ihmos != null) {
						ihmrs = new ArrayList<IhMRequest>();
						for (IhMOrder ihmo : ihmos) {
							IhMRequest ihmr = mRequestEndpoint.getIhMRequest(ihmo.getMRequestId()).execute();
							ihmrs.add(ihmr);
						}
					}
					exAdpt = new Ih_HelperOrderAdapter(getActivity(), ihmrs, ihmwids);
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(List<IhMRequest> result) {
				super.onPostExecute(result);
				exList.setIndicatorBounds(0, 20);
				exList.setAdapter(exAdpt);
				dialog.dismiss();
			}

		};
		task.execute();
		return rootView;
	}

	private class Ih_HelperOrderAdapter extends Ih_MessageAdapter {
		Context context;
		List<IhMRequest> datas;
		List<String> ihmwids;

		public Ih_HelperOrderAdapter(Context context, List<IhMRequest> datas) {
			super(context, datas);
			this.context = context;
			this.datas = datas;
		}

		public Ih_HelperOrderAdapter(Context context, List<IhMRequest> datas, List<String> ihmwids) {
			this(context, datas);
			this.ihmwids = ihmwids;
		}

		class ViewChildHolder {
			TextView itemLabel;
			Spinner itemWorkerList;
			Button itemAssign;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
				ViewGroup parent) {
			super.getChildView(groupPosition, childPosition, isLastChild, convertView, parent);
			View v = convertView;
			final ViewChildHolder vh;

			if (v == null) {
				v = super.getChildView(groupPosition, childPosition, isLastChild, convertView, parent);
				vh = new ViewChildHolder();
				if (ihmwids != null) {
					LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
					LinearLayout layout = new LinearLayout(context);
					layout.setLayoutParams(params);
					layout.setOrientation(LinearLayout.VERTICAL);

					vh.itemLabel = new TextView(context);
					vh.itemLabel.setLayoutParams(params);
					vh.itemLabel.setText("Worker list:");
					vh.itemWorkerList = new Spinner(context);
					vh.itemWorkerList.setLayoutParams(params);
					vh.itemWorkerList.setAdapter(
							new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, ihmwids));
					layout.addView(vh.itemWorkerList);
					vh.itemAssign = new Button(context);
					vh.itemAssign.setText("Assign Work");
					vh.itemAssign.setLayoutParams(params);
					layout.addView(vh.itemAssign);
					Ih_Tools.replaceView(v.findViewById(R.id.messageadapter_quotaion), layout);
				}
				tags.add(1, vh);

			} else {
				vh = (ViewChildHolder) tags.get(1);
			}
			if (ihmwids != null) {
				vh.itemAssign.setEnabled(true);
				vh.itemWorkerList.setEnabled(true);
				final IhMRequest ihmrequest = datas.get(groupPosition);
				OnClickListener assignworker = new OnClickListener() {
					@Override
					public void onClick(View v) {
						final Long mOrderId = ihmrequest.getMOrderId();
						final Long mWorkerId = Long.parseLong(vh.itemWorkerList.getSelectedItem().toString());
						AsyncTask<Void, Void, IhMOrder> task = new AsyncTask<Void, Void, IhMOrder>() {
							Ihmorderendpoint ihmoEndpoint = Ih_Apps.getmOrderEndpoint();
							IhMOrder order;

							@Override
							protected IhMOrder doInBackground(Void... params) {
								try {
									order = ihmoEndpoint.getIhMOrder(mOrderId).execute();
									if (order != null) {
										order.setMStatus("Assigned");
										order.setMWorkerId(mWorkerId);
										ihmoEndpoint.updateIhMOrder(order).execute();
									}
								} catch (IOException e) {
									e.printStackTrace();
								}
								return order;
							}

							@Override
							protected void onPostExecute(IhMOrder result) {
								super.onPostExecute(result);
								if (result != null) {
									vh.itemAssign.setEnabled(false);
									vh.itemWorkerList.setEnabled(false);
									Toast.makeText(mContext, "Order:" + mOrderId + "Assigned to" + mWorkerId,
											Toast.LENGTH_SHORT).show();
								}

							}

						};
						task.execute();
					}

				};

				vh.itemAssign.setOnClickListener(assignworker);
			}

			return v;
		}

	}
}
