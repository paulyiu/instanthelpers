package vtc.project.instanthelpers;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import vtc.project.instanthelpers.ihmaccountendpoint.Ihmaccountendpoint;
import vtc.project.instanthelpers.ihmaccountendpoint.model.IhMAccount;
import vtc.project.instanthelpers.ihmcustomerendpoint.Ihmcustomerendpoint;
import vtc.project.instanthelpers.ihmhelperendpoint.Ihmhelperendpoint;
import vtc.project.instanthelpers.ihmorderendpoint.Ihmorderendpoint;
import vtc.project.instanthelpers.ihmquotationendpoint.Ihmquotationendpoint;
import vtc.project.instanthelpers.ihmrequestendpoint.Ihmrequestendpoint;
import vtc.project.instanthelpers.ihmworkerendpoint.Ihmworkerendpoint;

public class Ih_Apps extends android.app.Application {

	private static Ih_Apps instance;
	private static GoogleApiClient mGoogleApiClient;
	private static GoogleAccountCredential credential;
	private static String accountName;
	private static IhMAccount ihmaccount;
	private static Ihmaccountendpoint mAccountEndpoint;
	private static Ihmrequestendpoint mRequestEndpoint;
	private static Ihmorderendpoint mOrderEndpoint;
	private static Ihmquotationendpoint mQuotationEndpoint;
	private static Ihmcustomerendpoint mCustomerEndpoint;
	private static Ihmhelperendpoint mHelperEndpoint;
	private static Ihmworkerendpoint mWorkerEndpoint;

	public static Ih_Apps getInstance() {
		if (instance == null) {
			instance = new Ih_Apps();
		}
		return instance;
	}

	public void onCreate() {
		super.onCreate();
		instance = this;
	}

	public static Ihmcustomerendpoint getmCustomerEndpoint() {
		return mCustomerEndpoint;
	}

	public static void setmCustomerEndpoint(Ihmcustomerendpoint mCustomerEndpoint) {
		Ih_Apps.mCustomerEndpoint = mCustomerEndpoint;
	}

	public static Ihmhelperendpoint getmHelperEndpoint() {
		return mHelperEndpoint;
	}

	public static void setmHelperEndpoint(Ihmhelperendpoint mHelperEndpoint) {
		Ih_Apps.mHelperEndpoint = mHelperEndpoint;
	}

	public static Ihmworkerendpoint getmWorkerEndpoint() {
		return mWorkerEndpoint;
	}

	public static void setmWorkerEndpoint(Ihmworkerendpoint mWorkerEndpoint) {
		Ih_Apps.mWorkerEndpoint = mWorkerEndpoint;
	}

	public static Ihmrequestendpoint getmRequestEndpoint() {
		return mRequestEndpoint;
	}

	public static void setmRequestEndpoint(Ihmrequestendpoint mRequestEndpoint) {
		Ih_Apps.mRequestEndpoint = mRequestEndpoint;
	}

	public static Ihmorderendpoint getmOrderEndpoint() {
		return mOrderEndpoint;
	}

	public static void setmOrderEndpoint(Ihmorderendpoint mOrderEndpoint) {
		Ih_Apps.mOrderEndpoint = mOrderEndpoint;
	}

	public static Ihmquotationendpoint getmQuotationEndpoint() {
		return mQuotationEndpoint;
	}

	public static void setmQuotationEndpoint(Ihmquotationendpoint mQuotationEndpoint) {
		Ih_Apps.mQuotationEndpoint = mQuotationEndpoint;
	}

	public static GoogleAccountCredential getCredential() {
		return credential;
	}

	public static void setCredential(GoogleAccountCredential credential) {
		Ih_Apps.credential = credential;
	}

	public static String getAccountName() {
		return accountName;
	}

	public static void setAccountName(String accountName) {
		Ih_Apps.accountName = accountName;
	}

	public static IhMAccount getIhmaccount() {
		return ihmaccount;
	}

	public static void setIhmaccount(IhMAccount ihmaccount) {
		Ih_Apps.ihmaccount = ihmaccount;
	}

	public static Ihmaccountendpoint getmAccountEndpoint() {
		return mAccountEndpoint;
	}

	public static void setmAccountEndpoint(Ihmaccountendpoint mAccountEndpoint) {
		Ih_Apps.mAccountEndpoint = mAccountEndpoint;
	}

	public static void setInstance(Ih_Apps instance) {
		Ih_Apps.instance = instance;
	}

	public static GoogleApiClient getmGoogleApiClient() {
		return mGoogleApiClient;
	}

	public static void setmGoogleApiClient(GoogleApiClient mGoogleApiClient) {
		Ih_Apps.mGoogleApiClient = mGoogleApiClient;
	}

}
