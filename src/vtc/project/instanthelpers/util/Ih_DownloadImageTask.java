package vtc.project.instanthelpers.util;

import java.io.InputStream;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

public class Ih_DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	ImageView bmImage;
	ProgressDialog dialog;
	boolean showDialog;
	Context context;
	
	public Ih_DownloadImageTask(Context context,ImageView bmImage,boolean showDialog) {
		this.bmImage = bmImage;
		this.showDialog = showDialog;
		this.context = context;
	}

	protected Bitmap doInBackground(String... urls) {
		String urldisplay = urls[0];
		Bitmap mIcon11 = null;
		if (urldisplay != null) {
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
		}
		return mIcon11;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog = new ProgressDialog(context);
		dialog.setCancelable(false);
		dialog.setTitle("Progress");
		dialog.setMessage("Downloading photo...");
		if(showDialog) dialog.show();

	}

	protected void onPostExecute(Bitmap result) {
		bmImage.setImageBitmap(result);
		if(dialog.isShowing())dialog.dismiss();
	}
}
