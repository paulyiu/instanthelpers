package vtc.project.instanthelpers.util;

public class Ih_Constants {
	public static final String WEB_CLIENT_ID = "221418896259-p9tge9ghm9mmq86vf4dtvbs94f0ceaka.apps.googleusercontent.com";
	public static final String ANDROID_CLIENT_ID = "221418896259-olrtbq2u025lo1se7fi9a3cnk1kcfl8i.apps.googleusercontent.com";
	public static final String IOS_CLIENT_ID = "221418896259-olrtbq2u025lo1se7fi9a3cnk1kcfl8i.apps.googleusercontent.com";
	public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;
	
	public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
	static final int REQUEST_ACCOUNT_PICKER = 1;
}