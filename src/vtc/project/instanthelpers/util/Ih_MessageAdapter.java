package vtc.project.instanthelpers.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import vtc.project.instanthelpers.Ih_Apps;
import vtc.project.instanthelpers.R;
import vtc.project.instanthelpers.ihmquotationendpoint.Ihmquotationendpoint;
import vtc.project.instanthelpers.ihmquotationendpoint.model.IhMQuotation;
import vtc.project.instanthelpers.ihmrequestendpoint.model.IhMRequest;

public class Ih_MessageAdapter extends BaseExpandableListAdapter {
	Context context;
	List<IhMRequest> datas;
	String[] mOrderType = { "Air", "Fire", "Water", "Electric" };
	int[] mOrderTypeIcon = { R.drawable.air, R.drawable.fire, R.drawable.water, R.drawable.electric };
	Bitmap[] bm;
	boolean[] downloads;
	String[] mRegion = { "Hong Kong", "Kowloon", "New Territories" };
	protected ArrayList<Object> tags;
	class ViewChildHolder {
		TextView itemTimestamp;
		EditText itemComment;
		EditText itemRegion;
		EditText itemAddress;
		ImageView itemPhoto;
		ListView itemQuota;
	}

	class ViewGroupHolder {
		TextView groupId;
		ImageView groupType;
	}

	public Ih_MessageAdapter(Context context, List<IhMRequest> datas) {
		this.context = context;
		this.datas = datas;
		if (datas != null) {
			this.bm = new Bitmap[datas.size() + 1];
			this.downloads = new boolean[datas.size() + 1];
		}

	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewGroupHolder vh;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.listviewgroup_messageadapter, parent, false);
			vh = new ViewGroupHolder();
			vh.groupId = (TextView) v.findViewById(R.id.messageadapter_group_id);
			vh.groupType = (ImageView) v.findViewById(R.id.messageadapter_group_type);
			v.setTag(vh);
		} else {
			vh = (ViewGroupHolder) v.getTag();
		}
		if (datas != null) {
			IhMRequest ihmrequest = datas.get(groupPosition);
			vh.groupId.setText("" + ihmrequest.getId());
			vh.groupType
					.setImageDrawable(context.getResources().getDrawable(mOrderTypeIcon[ihmrequest.getMOrderType()]));
		}
		return v;
	}

	@Override
	public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {
		View v = convertView;
		final ViewChildHolder vh;
		final IhMRequest ihmrequest = datas.get(groupPosition);
		if (v == null) {
			tags = new ArrayList<Object>();
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.listviewitem_messageadapter, parent, false);
			vh = new ViewChildHolder();
			vh.itemTimestamp = (TextView) v.findViewById(R.id.messageadapter_item_timestamp);
			vh.itemComment = (EditText) v.findViewById(R.id.messageadapter_item_comment2);
			vh.itemRegion = (EditText) v.findViewById(R.id.messageadapter_item_region2);
			vh.itemAddress = (EditText) v.findViewById(R.id.messageadapter_item_address2);
			vh.itemPhoto = (ImageView) v.findViewById(R.id.messageadapter_item_photo);
			vh.itemQuota = (ListView) v.findViewById(R.id.messageadapter_quotaion);
			tags.add(vh);
		} else {
			vh = (ViewChildHolder) tags.get(0);
		}

		Calendar cal = Calendar.getInstance(Locale.ENGLISH);
		cal.setTimeInMillis(ihmrequest.getTimestamp());
		String date = DateFormat.format("dd-MM-yyyy", cal).toString();
		vh.itemTimestamp.setText(date);
		vh.itemComment.setText(ihmrequest.getMComment());
		vh.itemRegion.setText(mRegion[Integer.parseInt(ihmrequest.getMRegion())]);
		vh.itemAddress.setText(ihmrequest.getMAddress());
		String url = null;
		final float scale = context.getResources().getDisplayMetrics().density;
		if (ihmrequest.getMPhoto() != null) {
			url = "http://instant-helpers.appspot.com/serve?blob-key=" + ihmrequest.getMPhoto().get(0);
			vh.itemPhoto.getLayoutParams().height = (int) (150 * scale + 0.5f);
		} else {
			vh.itemPhoto.getLayoutParams().height = 0;
		}

		String[] args = { url, "" + groupPosition, "" + ihmrequest.getId() };

		vh.itemPhoto.setImageBitmap(bm[groupPosition]);
		if (!downloads[groupPosition]) {
			new Ih_DownloadImageTask(context, vh.itemPhoto, false).execute(args);
			AsyncTask<Long, String, List<IhMQuotation>> task = new AsyncTask<Long, String, List<IhMQuotation>>() {
				Ihmquotationendpoint ihmqEndpoint = Ih_Apps.getmQuotationEndpoint();
				int position;

				@Override
				protected List<IhMQuotation> doInBackground(Long... params) {
					List<IhMQuotation> ihmq = null;
					position = params[2].intValue();
					try {
						ihmq = ihmqEndpoint.getIhMQuotationByRequestAndHelperId(params[0], params[1]).execute()
								.getItems();
					} catch (IOException e) {
						e.printStackTrace();
					}
					return ihmq;
				}

				@Override
				protected void onPostExecute(List<IhMQuotation> result) {
					super.onPostExecute(result);
				}

			};
			task.execute(Ih_Apps.getIhmaccount().getId(), ihmrequest.getId(), Long.parseLong("" + groupPosition));
		}

		return v;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public int getGroupCount() {
		return datas != null ? datas.size() : 0;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return datas != null ? datas.get(groupPosition) : null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return datas != null ? datas.get(groupPosition) : null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return datas != null ? datas.get(groupPosition).hashCode() : null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return datas != null ? datas.get(groupPosition).hashCode() : null;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

}
