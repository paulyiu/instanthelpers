package vtc.project.instanthelpers.util;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

public class Ih_QuotaList extends ListView {
	private int rows;
	private float scale;
	private final int ROW_HEIGHT = 150;

	public Ih_QuotaList(Context context) {
		super(context);
	}

	public Ih_QuotaList(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setRows(int rows, float scale) {
		this.rows = rows;
		this.scale = scale;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(getMeasuredWidth(), rows * (int)(ROW_HEIGHT * scale));
		
        Log.d( "QuotaLayout", "onMeasure "+this+
                ": width: "+decodeMeasureSpec( widthMeasureSpec )+
                "; height: "+decodeMeasureSpec( heightMeasureSpec )+
                "; measuredHeight: "+getMeasuredHeight()+
                "; measuredWidth: "+getMeasuredWidth() );
	}
	  protected void onLayout (boolean changed, int left, int top, int right, int bottom) {
	        super.onLayout( changed, left,top,right,bottom );
	        Log.d( "QuotaLayout","onLayout "+this+": changed: "+changed+"; left: "+left+"; top: "+top+"; right: "+right+"; bottom: "+bottom );
	    }
	 
	    private String decodeMeasureSpec( int measureSpec ) {
	        int mode = View.MeasureSpec.getMode( measureSpec );
	        String modeString = "<> ";
	        switch( mode ) {
	            case View.MeasureSpec.UNSPECIFIED:
	                modeString = "UNSPECIFIED ";
	                break;

	            case View.MeasureSpec.EXACTLY:
	                modeString = "EXACTLY ";
	                break;

	            case View.MeasureSpec.AT_MOST:
	                modeString = "AT_MOST ";
	                break;
	        }
	        return modeString+Integer.toString( View.MeasureSpec.getSize( measureSpec ) );
	    }
}
