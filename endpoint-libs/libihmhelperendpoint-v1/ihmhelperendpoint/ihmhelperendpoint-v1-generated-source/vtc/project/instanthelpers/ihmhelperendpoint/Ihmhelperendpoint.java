/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * This code was generated by https://code.google.com/p/google-apis-client-generator/
 * (build: 2015-05-05 20:00:12 UTC)
 * on 2015-05-08 at 14:43:52 UTC 
 * Modify at your own risk.
 */

package vtc.project.instanthelpers.ihmhelperendpoint;

/**
 * Service definition for Ihmhelperendpoint (v1).
 *
 * <p>
 * This is an API
 * </p>
 *
 * <p>
 * For more information about this service, see the
 * <a href="" target="_blank">API Documentation</a>
 * </p>
 *
 * <p>
 * This service uses {@link IhmhelperendpointRequestInitializer} to initialize global parameters via its
 * {@link Builder}.
 * </p>
 *
 * @since 1.3
 * @author Google, Inc.
 */
@SuppressWarnings("javadoc")
public class Ihmhelperendpoint extends com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient {

  // Note: Leave this static initializer at the top of the file.
  static {
    com.google.api.client.util.Preconditions.checkState(
        com.google.api.client.googleapis.GoogleUtils.MAJOR_VERSION == 1 &&
        com.google.api.client.googleapis.GoogleUtils.MINOR_VERSION >= 15,
        "You are currently running with version %s of google-api-client. " +
        "You need at least version 1.15 of google-api-client to run version " +
        "1.18.0-rc of the ihmhelperendpoint library.", com.google.api.client.googleapis.GoogleUtils.VERSION);
  }

  /**
   * The default encoded root URL of the service. This is determined when the library is generated
   * and normally should not be changed.
   *
   * @since 1.7
   */
  public static final String DEFAULT_ROOT_URL = "https://instant-helpers.appspot.com/_ah/api/";

  /**
   * The default encoded service path of the service. This is determined when the library is
   * generated and normally should not be changed.
   *
   * @since 1.7
   */
  public static final String DEFAULT_SERVICE_PATH = "ihmhelperendpoint/v1/";

  /**
   * The default encoded base URL of the service. This is determined when the library is generated
   * and normally should not be changed.
   */
  public static final String DEFAULT_BASE_URL = DEFAULT_ROOT_URL + DEFAULT_SERVICE_PATH;

  /**
   * Constructor.
   *
   * <p>
   * Use {@link Builder} if you need to specify any of the optional parameters.
   * </p>
   *
   * @param transport HTTP transport, which should normally be:
   *        <ul>
   *        <li>Google App Engine:
   *        {@code com.google.api.client.extensions.appengine.http.UrlFetchTransport}</li>
   *        <li>Android: {@code newCompatibleTransport} from
   *        {@code com.google.api.client.extensions.android.http.AndroidHttp}</li>
   *        <li>Java: {@link com.google.api.client.googleapis.javanet.GoogleNetHttpTransport#newTrustedTransport()}
   *        </li>
   *        </ul>
   * @param jsonFactory JSON factory, which may be:
   *        <ul>
   *        <li>Jackson: {@code com.google.api.client.json.jackson2.JacksonFactory}</li>
   *        <li>Google GSON: {@code com.google.api.client.json.gson.GsonFactory}</li>
   *        <li>Android Honeycomb or higher:
   *        {@code com.google.api.client.extensions.android.json.AndroidJsonFactory}</li>
   *        </ul>
   * @param httpRequestInitializer HTTP request initializer or {@code null} for none
   * @since 1.7
   */
  public Ihmhelperendpoint(com.google.api.client.http.HttpTransport transport, com.google.api.client.json.JsonFactory jsonFactory,
      com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
    this(new Builder(transport, jsonFactory, httpRequestInitializer));
  }

  /**
   * @param builder builder
   */
  Ihmhelperendpoint(Builder builder) {
    super(builder);
  }

  @Override
  protected void initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest<?> httpClientRequest) throws java.io.IOException {
    super.initialize(httpClientRequest);
  }

  /**
   * Create a request for the method "getIhMHelper".
   *
   * This request holds the parameters needed by the ihmhelperendpoint server.  After setting any
   * optional parameters, call the {@link GetIhMHelper#execute()} method to invoke the remote
   * operation.
   *
   * @param id
   * @return the request
   */
  public GetIhMHelper getIhMHelper(java.lang.Long id) throws java.io.IOException {
    GetIhMHelper result = new GetIhMHelper(id);
    initialize(result);
    return result;
  }

  public class GetIhMHelper extends IhmhelperendpointRequest<vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper> {

    private static final String REST_PATH = "ihmhelper/{id}";

    /**
     * Create a request for the method "getIhMHelper".
     *
     * This request holds the parameters needed by the the ihmhelperendpoint server.  After setting
     * any optional parameters, call the {@link GetIhMHelper#execute()} method to invoke the remote
     * operation. <p> {@link
     * GetIhMHelper#initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest)}
     * must be called to initialize this instance immediately after invoking the constructor. </p>
     *
     * @param id
     * @since 1.13
     */
    protected GetIhMHelper(java.lang.Long id) {
      super(Ihmhelperendpoint.this, "GET", REST_PATH, null, vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper.class);
      this.id = com.google.api.client.util.Preconditions.checkNotNull(id, "Required parameter id must be specified.");
    }

    @Override
    public com.google.api.client.http.HttpResponse executeUsingHead() throws java.io.IOException {
      return super.executeUsingHead();
    }

    @Override
    public com.google.api.client.http.HttpRequest buildHttpRequestUsingHead() throws java.io.IOException {
      return super.buildHttpRequestUsingHead();
    }

    @Override
    public GetIhMHelper setAlt(java.lang.String alt) {
      return (GetIhMHelper) super.setAlt(alt);
    }

    @Override
    public GetIhMHelper setFields(java.lang.String fields) {
      return (GetIhMHelper) super.setFields(fields);
    }

    @Override
    public GetIhMHelper setKey(java.lang.String key) {
      return (GetIhMHelper) super.setKey(key);
    }

    @Override
    public GetIhMHelper setOauthToken(java.lang.String oauthToken) {
      return (GetIhMHelper) super.setOauthToken(oauthToken);
    }

    @Override
    public GetIhMHelper setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (GetIhMHelper) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public GetIhMHelper setQuotaUser(java.lang.String quotaUser) {
      return (GetIhMHelper) super.setQuotaUser(quotaUser);
    }

    @Override
    public GetIhMHelper setUserIp(java.lang.String userIp) {
      return (GetIhMHelper) super.setUserIp(userIp);
    }

    @com.google.api.client.util.Key
    private java.lang.Long id;

    /**

     */
    public java.lang.Long getId() {
      return id;
    }

    public GetIhMHelper setId(java.lang.Long id) {
      this.id = id;
      return this;
    }

    @Override
    public GetIhMHelper set(String parameterName, Object value) {
      return (GetIhMHelper) super.set(parameterName, value);
    }
  }

  /**
   * Create a request for the method "insertIhMHelper".
   *
   * This request holds the parameters needed by the ihmhelperendpoint server.  After setting any
   * optional parameters, call the {@link InsertIhMHelper#execute()} method to invoke the remote
   * operation.
   *
   * @param content the {@link vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper}
   * @return the request
   */
  public InsertIhMHelper insertIhMHelper(vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper content) throws java.io.IOException {
    InsertIhMHelper result = new InsertIhMHelper(content);
    initialize(result);
    return result;
  }

  public class InsertIhMHelper extends IhmhelperendpointRequest<vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper> {

    private static final String REST_PATH = "ihmhelper";

    /**
     * Create a request for the method "insertIhMHelper".
     *
     * This request holds the parameters needed by the the ihmhelperendpoint server.  After setting
     * any optional parameters, call the {@link InsertIhMHelper#execute()} method to invoke the remote
     * operation. <p> {@link InsertIhMHelper#initialize(com.google.api.client.googleapis.services.Abst
     * ractGoogleClientRequest)} must be called to initialize this instance immediately after invoking
     * the constructor. </p>
     *
     * @param content the {@link vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper}
     * @since 1.13
     */
    protected InsertIhMHelper(vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper content) {
      super(Ihmhelperendpoint.this, "POST", REST_PATH, content, vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper.class);
    }

    @Override
    public InsertIhMHelper setAlt(java.lang.String alt) {
      return (InsertIhMHelper) super.setAlt(alt);
    }

    @Override
    public InsertIhMHelper setFields(java.lang.String fields) {
      return (InsertIhMHelper) super.setFields(fields);
    }

    @Override
    public InsertIhMHelper setKey(java.lang.String key) {
      return (InsertIhMHelper) super.setKey(key);
    }

    @Override
    public InsertIhMHelper setOauthToken(java.lang.String oauthToken) {
      return (InsertIhMHelper) super.setOauthToken(oauthToken);
    }

    @Override
    public InsertIhMHelper setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (InsertIhMHelper) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public InsertIhMHelper setQuotaUser(java.lang.String quotaUser) {
      return (InsertIhMHelper) super.setQuotaUser(quotaUser);
    }

    @Override
    public InsertIhMHelper setUserIp(java.lang.String userIp) {
      return (InsertIhMHelper) super.setUserIp(userIp);
    }

    @Override
    public InsertIhMHelper set(String parameterName, Object value) {
      return (InsertIhMHelper) super.set(parameterName, value);
    }
  }

  /**
   * Create a request for the method "listIhMHelper".
   *
   * This request holds the parameters needed by the ihmhelperendpoint server.  After setting any
   * optional parameters, call the {@link ListIhMHelper#execute()} method to invoke the remote
   * operation.
   *
   * @return the request
   */
  public ListIhMHelper listIhMHelper() throws java.io.IOException {
    ListIhMHelper result = new ListIhMHelper();
    initialize(result);
    return result;
  }

  public class ListIhMHelper extends IhmhelperendpointRequest<vtc.project.instanthelpers.ihmhelperendpoint.model.CollectionResponseIhMHelper> {

    private static final String REST_PATH = "ihmhelper";

    /**
     * Create a request for the method "listIhMHelper".
     *
     * This request holds the parameters needed by the the ihmhelperendpoint server.  After setting
     * any optional parameters, call the {@link ListIhMHelper#execute()} method to invoke the remote
     * operation. <p> {@link ListIhMHelper#initialize(com.google.api.client.googleapis.services.Abstra
     * ctGoogleClientRequest)} must be called to initialize this instance immediately after invoking
     * the constructor. </p>
     *
     * @since 1.13
     */
    protected ListIhMHelper() {
      super(Ihmhelperendpoint.this, "GET", REST_PATH, null, vtc.project.instanthelpers.ihmhelperendpoint.model.CollectionResponseIhMHelper.class);
    }

    @Override
    public com.google.api.client.http.HttpResponse executeUsingHead() throws java.io.IOException {
      return super.executeUsingHead();
    }

    @Override
    public com.google.api.client.http.HttpRequest buildHttpRequestUsingHead() throws java.io.IOException {
      return super.buildHttpRequestUsingHead();
    }

    @Override
    public ListIhMHelper setAlt(java.lang.String alt) {
      return (ListIhMHelper) super.setAlt(alt);
    }

    @Override
    public ListIhMHelper setFields(java.lang.String fields) {
      return (ListIhMHelper) super.setFields(fields);
    }

    @Override
    public ListIhMHelper setKey(java.lang.String key) {
      return (ListIhMHelper) super.setKey(key);
    }

    @Override
    public ListIhMHelper setOauthToken(java.lang.String oauthToken) {
      return (ListIhMHelper) super.setOauthToken(oauthToken);
    }

    @Override
    public ListIhMHelper setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (ListIhMHelper) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public ListIhMHelper setQuotaUser(java.lang.String quotaUser) {
      return (ListIhMHelper) super.setQuotaUser(quotaUser);
    }

    @Override
    public ListIhMHelper setUserIp(java.lang.String userIp) {
      return (ListIhMHelper) super.setUserIp(userIp);
    }

    @com.google.api.client.util.Key
    private java.lang.String cursor;

    /**

     */
    public java.lang.String getCursor() {
      return cursor;
    }

    public ListIhMHelper setCursor(java.lang.String cursor) {
      this.cursor = cursor;
      return this;
    }

    @com.google.api.client.util.Key
    private java.lang.Integer limit;

    /**

     */
    public java.lang.Integer getLimit() {
      return limit;
    }

    public ListIhMHelper setLimit(java.lang.Integer limit) {
      this.limit = limit;
      return this;
    }

    @Override
    public ListIhMHelper set(String parameterName, Object value) {
      return (ListIhMHelper) super.set(parameterName, value);
    }
  }

  /**
   * Create a request for the method "removeIhMHelper".
   *
   * This request holds the parameters needed by the ihmhelperendpoint server.  After setting any
   * optional parameters, call the {@link RemoveIhMHelper#execute()} method to invoke the remote
   * operation.
   *
   * @param id
   * @return the request
   */
  public RemoveIhMHelper removeIhMHelper(java.lang.Long id) throws java.io.IOException {
    RemoveIhMHelper result = new RemoveIhMHelper(id);
    initialize(result);
    return result;
  }

  public class RemoveIhMHelper extends IhmhelperendpointRequest<Void> {

    private static final String REST_PATH = "ihmhelper/{id}";

    /**
     * Create a request for the method "removeIhMHelper".
     *
     * This request holds the parameters needed by the the ihmhelperendpoint server.  After setting
     * any optional parameters, call the {@link RemoveIhMHelper#execute()} method to invoke the remote
     * operation. <p> {@link RemoveIhMHelper#initialize(com.google.api.client.googleapis.services.Abst
     * ractGoogleClientRequest)} must be called to initialize this instance immediately after invoking
     * the constructor. </p>
     *
     * @param id
     * @since 1.13
     */
    protected RemoveIhMHelper(java.lang.Long id) {
      super(Ihmhelperendpoint.this, "DELETE", REST_PATH, null, Void.class);
      this.id = com.google.api.client.util.Preconditions.checkNotNull(id, "Required parameter id must be specified.");
    }

    @Override
    public RemoveIhMHelper setAlt(java.lang.String alt) {
      return (RemoveIhMHelper) super.setAlt(alt);
    }

    @Override
    public RemoveIhMHelper setFields(java.lang.String fields) {
      return (RemoveIhMHelper) super.setFields(fields);
    }

    @Override
    public RemoveIhMHelper setKey(java.lang.String key) {
      return (RemoveIhMHelper) super.setKey(key);
    }

    @Override
    public RemoveIhMHelper setOauthToken(java.lang.String oauthToken) {
      return (RemoveIhMHelper) super.setOauthToken(oauthToken);
    }

    @Override
    public RemoveIhMHelper setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (RemoveIhMHelper) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public RemoveIhMHelper setQuotaUser(java.lang.String quotaUser) {
      return (RemoveIhMHelper) super.setQuotaUser(quotaUser);
    }

    @Override
    public RemoveIhMHelper setUserIp(java.lang.String userIp) {
      return (RemoveIhMHelper) super.setUserIp(userIp);
    }

    @com.google.api.client.util.Key
    private java.lang.Long id;

    /**

     */
    public java.lang.Long getId() {
      return id;
    }

    public RemoveIhMHelper setId(java.lang.Long id) {
      this.id = id;
      return this;
    }

    @Override
    public RemoveIhMHelper set(String parameterName, Object value) {
      return (RemoveIhMHelper) super.set(parameterName, value);
    }
  }

  /**
   * Create a request for the method "updateIhMHelper".
   *
   * This request holds the parameters needed by the ihmhelperendpoint server.  After setting any
   * optional parameters, call the {@link UpdateIhMHelper#execute()} method to invoke the remote
   * operation.
   *
   * @param content the {@link vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper}
   * @return the request
   */
  public UpdateIhMHelper updateIhMHelper(vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper content) throws java.io.IOException {
    UpdateIhMHelper result = new UpdateIhMHelper(content);
    initialize(result);
    return result;
  }

  public class UpdateIhMHelper extends IhmhelperendpointRequest<vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper> {

    private static final String REST_PATH = "ihmhelper";

    /**
     * Create a request for the method "updateIhMHelper".
     *
     * This request holds the parameters needed by the the ihmhelperendpoint server.  After setting
     * any optional parameters, call the {@link UpdateIhMHelper#execute()} method to invoke the remote
     * operation. <p> {@link UpdateIhMHelper#initialize(com.google.api.client.googleapis.services.Abst
     * ractGoogleClientRequest)} must be called to initialize this instance immediately after invoking
     * the constructor. </p>
     *
     * @param content the {@link vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper}
     * @since 1.13
     */
    protected UpdateIhMHelper(vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper content) {
      super(Ihmhelperendpoint.this, "PUT", REST_PATH, content, vtc.project.instanthelpers.ihmhelperendpoint.model.IhMHelper.class);
    }

    @Override
    public UpdateIhMHelper setAlt(java.lang.String alt) {
      return (UpdateIhMHelper) super.setAlt(alt);
    }

    @Override
    public UpdateIhMHelper setFields(java.lang.String fields) {
      return (UpdateIhMHelper) super.setFields(fields);
    }

    @Override
    public UpdateIhMHelper setKey(java.lang.String key) {
      return (UpdateIhMHelper) super.setKey(key);
    }

    @Override
    public UpdateIhMHelper setOauthToken(java.lang.String oauthToken) {
      return (UpdateIhMHelper) super.setOauthToken(oauthToken);
    }

    @Override
    public UpdateIhMHelper setPrettyPrint(java.lang.Boolean prettyPrint) {
      return (UpdateIhMHelper) super.setPrettyPrint(prettyPrint);
    }

    @Override
    public UpdateIhMHelper setQuotaUser(java.lang.String quotaUser) {
      return (UpdateIhMHelper) super.setQuotaUser(quotaUser);
    }

    @Override
    public UpdateIhMHelper setUserIp(java.lang.String userIp) {
      return (UpdateIhMHelper) super.setUserIp(userIp);
    }

    @Override
    public UpdateIhMHelper set(String parameterName, Object value) {
      return (UpdateIhMHelper) super.set(parameterName, value);
    }
  }

  /**
   * Builder for {@link Ihmhelperendpoint}.
   *
   * <p>
   * Implementation is not thread-safe.
   * </p>
   *
   * @since 1.3.0
   */
  public static final class Builder extends com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder {

    /**
     * Returns an instance of a new builder.
     *
     * @param transport HTTP transport, which should normally be:
     *        <ul>
     *        <li>Google App Engine:
     *        {@code com.google.api.client.extensions.appengine.http.UrlFetchTransport}</li>
     *        <li>Android: {@code newCompatibleTransport} from
     *        {@code com.google.api.client.extensions.android.http.AndroidHttp}</li>
     *        <li>Java: {@link com.google.api.client.googleapis.javanet.GoogleNetHttpTransport#newTrustedTransport()}
     *        </li>
     *        </ul>
     * @param jsonFactory JSON factory, which may be:
     *        <ul>
     *        <li>Jackson: {@code com.google.api.client.json.jackson2.JacksonFactory}</li>
     *        <li>Google GSON: {@code com.google.api.client.json.gson.GsonFactory}</li>
     *        <li>Android Honeycomb or higher:
     *        {@code com.google.api.client.extensions.android.json.AndroidJsonFactory}</li>
     *        </ul>
     * @param httpRequestInitializer HTTP request initializer or {@code null} for none
     * @since 1.7
     */
    public Builder(com.google.api.client.http.HttpTransport transport, com.google.api.client.json.JsonFactory jsonFactory,
        com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
      super(
          transport,
          jsonFactory,
          DEFAULT_ROOT_URL,
          DEFAULT_SERVICE_PATH,
          httpRequestInitializer,
          false);
    }

    /** Builds a new instance of {@link Ihmhelperendpoint}. */
    @Override
    public Ihmhelperendpoint build() {
      return new Ihmhelperendpoint(this);
    }

    @Override
    public Builder setRootUrl(String rootUrl) {
      return (Builder) super.setRootUrl(rootUrl);
    }

    @Override
    public Builder setServicePath(String servicePath) {
      return (Builder) super.setServicePath(servicePath);
    }

    @Override
    public Builder setHttpRequestInitializer(com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
      return (Builder) super.setHttpRequestInitializer(httpRequestInitializer);
    }

    @Override
    public Builder setApplicationName(String applicationName) {
      return (Builder) super.setApplicationName(applicationName);
    }

    @Override
    public Builder setSuppressPatternChecks(boolean suppressPatternChecks) {
      return (Builder) super.setSuppressPatternChecks(suppressPatternChecks);
    }

    @Override
    public Builder setSuppressRequiredParameterChecks(boolean suppressRequiredParameterChecks) {
      return (Builder) super.setSuppressRequiredParameterChecks(suppressRequiredParameterChecks);
    }

    @Override
    public Builder setSuppressAllChecks(boolean suppressAllChecks) {
      return (Builder) super.setSuppressAllChecks(suppressAllChecks);
    }

    /**
     * Set the {@link IhmhelperendpointRequestInitializer}.
     *
     * @since 1.12
     */
    public Builder setIhmhelperendpointRequestInitializer(
        IhmhelperendpointRequestInitializer ihmhelperendpointRequestInitializer) {
      return (Builder) super.setGoogleClientRequestInitializer(ihmhelperendpointRequestInitializer);
    }

    @Override
    public Builder setGoogleClientRequestInitializer(
        com.google.api.client.googleapis.services.GoogleClientRequestInitializer googleClientRequestInitializer) {
      return (Builder) super.setGoogleClientRequestInitializer(googleClientRequestInitializer);
    }
  }
}
